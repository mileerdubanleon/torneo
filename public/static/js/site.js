/* *--------------------------------------------------------------
# List products
--------------------------------------------------------------*/
var products_list; // Declaración global
const base = location.protocol + '//' + location.host;
const route = document.getElementsByName('routeName')[0].getAttribute('content');
const http = new XMLHttpRequest(); // Product list
const csrfToken = document.getElementsByName('csrf-token')[0].getAttribute('content'); // Product list
const currency = document.getElementsByName('currency')[0].getAttribute('content');
const auth = document.getElementsByName('auth')[0].getAttribute('content');
var page = 1;
var page_section = "";
var products_list_ids_temp = [];

document.addEventListener('DOMContentLoaded', function() {

    products_list = document.getElementById('products_list');
    var load_more_products = document.getElementById('load_more_products');

    if (load_more_products) {
        load_more_products.addEventListener('click', function(e) {
            e.preventDefault();
            load_products(page_section);
        });
    }

    if (route == "home") {
        load_products('home');
    }
    if (route == "store") {
        load_products('store');
    }
    if (route == "store_category") {
        let object_id = document.getElementsByName('category_id')[0].getAttribute('content');
        load_products('store_category', object_id);
    }

    function load_products(section, object) {
        page_section = section;
        var url = base + '/md/api/load/products/' + page_section + '?page=' + page + '&object_id=' + object;
        http.open('GET', url, true); // abrimos conexión
        http.setRequestHeader('X-CSRF-TOKEN', csrfToken); // evitar error de token
        http.send(); // ejecutar
        http.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                page = page + 1;
                var data = this.responseText;
                console.log(data);
                data = JSON.parse(data);

                if (data.data.length == 0) {
                    load_more_products.style.display = "none";
                    // Mostrar mensaje cuando no hay productos
                    products_list.innerHTML = "<div class='text-center w-100'><p class='my-4 h5 lead'>No hay productos públicos o no hay productos almacenados</p></div>";
                    return;
                }

                // Function to load products
                data.data.forEach(function(product, index) {
                    products_list_ids_temp.push(product.id);
                    var div = "";
                    div += "<div class=\"item-product \">";
                    div += "<img src=\"" + base + "/storage/img/uploads_product_image/" + product.image + "\" alt=\"" + product.name + "\">";
                    div += "<div class=\"d-flex justify-content-center\">";
                        div += "<span class=\"my-2 h5 mx-2\" style=\"font-weight: 600;\">" + product.name + "</span>";
                        div += "<span class=\"my-2 mx-3\">" + currency + product.price + "</span>";
                    div += "</div>";

                    div += "<div class=\"mx-1\">";
                    div += "<div class=\"sizes my-2\">";
                    div += "<button class=\"size-btn\">S</button>";
                    div += "<button class=\"size-btn\">M</button>";
                    div += "<button class=\"size-btn\">L</button>";
                    div += "<button class=\"size-btn\">XL</button>";
                    div += "</div>";
                    div += "</div>";
                    div += "<div class=\"my-1 d-flex justify-content-center\">";
                    div += "<a href=\"#\" class=\"bg-dark py-2 text-white d-flex align-items-center px-4 text-center\">Agregar al carrito</a>";
                    if (auth == "1") {
                        div += "<a href=\"#\" class=\"hover-favorite\" id=\"favorite_1_" + product.id + "\" onclick=\"add_to_favorites('" + product.id + "', '1'); return false;\">";
                    } else {
                        div += "<a href=\"#\" class=\"hover-favorite\" id=\"favorite_1_" + product.id + "\" onclick=\"swal.fire({title: 'Oops...', text: 'Hola invitado :D, debes tener una cuenta creada para seleccionar este producto como favorito.', icon: 'warning'}); return false;\">";
                    }
                    div += "<i class=\"bi bi-heart px-4 my-3 d-flex align-items-center text-center text-favorite text-dark\"></i>";
                    div += "</a>";
                    div += "</div>";
                    div += "</div>";

                    products_list.innerHTML += div;
                });

                if (auth == "1") {
                    mark_user_favorites(products_list_ids_temp);
                    products_list_ids_temp = [];
                    console.log(products_list_ids_temp);
                }
            } else {
                // Mostrar mensaje cuando no hay productos
                products_list.innerHTML = "<div class='text-center w-100'><p class='my-4 h5 lead'>No hay productos almacenados</p></div>";
                return;
            }
        }
    }

    function mark_user_favorites(objects) {
        var url = base + '/md/api/load/user/favorites/';
        var params = 'module=1&objects=' + objects;
        http.open('POST', url, true);
        http.setRequestHeader('X-CSRF-TOKEN', csrfToken);
        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        http.send(params);
        http.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var data = this.responseText;
                data = JSON.parse(data);
                if (data.count > "0") {
                    data.objects.forEach(function(favorite, index) {
                        document.getElementById('favorite_1_' + favorite).removeAttribute('onclick');
                        document.getElementById('favorite_1_' + favorite).classList.add('favorite_active');
                    });
                }
            }
        }
    }

    function add_to_favorites(object, module) {
        url = base + '/md/api/favorites/add/' + object + '/' + module;
        http.open('POST', url, true);
        http.setRequestHeader('X-CSRF-TOKEN', csrfToken);
        http.send();
        http.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var data = this.responseText;
                data = JSON.parse(data);
                if (data.status == "success") {
                    document.getElementById('favorite_' + module + '_' + object).removeAttribute('onclick');
                    document.getElementById('favorite_' + module + '_' + object).classList.add('favorite_active');
                }
            }
        }
    }
});
