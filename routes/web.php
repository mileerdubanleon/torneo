<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ConnectController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\GoogleController;
use App\Http\Controllers\FacebookController;
use App\Http\Controllers\ApiJsController;
use App\Http\Controllers\TournamentController;

Route::get('/', [HomeController::class, 'index'])->name('home');

// Route Auth
Route::get('/login', [ConnectController::class, 'getLogin'])->name('login');
Route::post('/login', [ConnectController::class, 'postLogin']);
Route::get('/register', [ConnectController::class, 'getRegister'])->name('register');
Route::post('/register', [ConnectController::class, 'postRegister']);
Route::get('/logout', [ConnectController::class, 'getLogout'])->name('logout');

// Route Google
Route::get('/auth/google', [GoogleController::class, 'redirectToGoogle'])->name('auth_google');
Route::get('/auth/google/callback', [GoogleController::class, 'handleGoogleCallback'])->name('auth.google.callback');
// Route Facebook
Route::get('/auth/facebook', [FacebookController::class, 'redirectTofacebook'])->name('auth_facebook');
Route::get('/auth/facebook/callback', [FacebookController::class, 'handleFacebookCallback'])->name('auth_facebook_callback');

// Module Users Edit
Route::get('/account/edit', [UserController::class, 'getAccountEdit'])->name('account_edit');
// Module avatar
Route::post('/account/edit/avatar', [UserController::class, 'postAccountAvatar'])->name('account_avatar_edit');
Route::post('/account/edit/avatar/delete', [UserController::class, 'deleteAccountAvatar'])->name('account_avatar_delete');

// Module password
Route::post('/account/edit/password', [UserController::class, 'postAccountPassword'])->name('account_password_edit');
Route::post('/account/edit/info', [UserController::class, 'postAccountInfo'])->name('account_info_edit');

// Module tournament
Route::get('/tournaments', [TournamentController::class, 'getTournaments'])->name('public_tournaments');
Route::get('/tournaments/teams/{id}', [HomeController::class, 'viewTournamentTeams'])->name('tournament_teams');



