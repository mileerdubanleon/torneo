<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\TournamentsController;
use App\Http\Controllers\Admin\TeamsController;
use App\Http\Controllers\Admin\PlayersController;
use App\Http\Controllers\Admin\GroupsController;
use App\Http\Controllers\Admin\MatchDetailController;

Route::prefix('/admin')->group(function () {
    // View dashboard admin
    Route::get('/', [DashboardController::class, 'getDashboard'])->name('dashboard');

    // Module Users
    // GET
    Route::get('/users/{status}', [UserController::class, 'getUsers'])->name('user_list');
    Route::get('/user/{id}/edit', [UserController::class, 'getUserEdit'])->name('user_edit_get');
    Route::get('/user/{id}/banned', [UserController::class, 'getUserBanned'])->name('user_banned');
    Route::get('/user/{id}/permissions', [UserController::class, 'getUserPermissions'])->name('user_permissions_get');
    // POST
    Route::post('/user/{id}/edit', [UserController::class, 'postUserEdit'])->name('user_edit');
    Route::post('/user/{id}/permissions', [UserController::class, 'postUserPermissions'])->name('user_permissions');

    // Module tournaments
    // GET
    Route::get('/tournaments', [TournamentsController::class, 'getTournaments'])->name('tournaments');
    Route::get('/tournaments/add', [TournamentsController::class, 'getTournamentsAdd'])->name('get_tournaments_add');
    Route::get('/tournaments/edit', [TournamentsController::class, 'getTournamentsEdit'])->name('get_tournaments_edit');
    // GET groups / teams
    Route::get('/tournaments/teams/{id}', [TournamentsController::class, 'viewTournamentTeams'])->name('tournament_group_teams');
    // POST
    Route::post('/tournaments/add', [TournamentsController::class, 'postTournamentsAdd'])->name('post_tournaments_add');
    Route::post('/tournaments/{id}/edit', [TournamentsController::class, 'postTournamentsEdit'])->name('post_tournaments_edit');

	// Module teams
    // GET
    Route::get('/teams', [TeamsController::class, 'getTeams'])->name('teams');
    Route::get('/teams/add', [TeamsController::class, 'getTeamsAdd'])->name('get_teams_add');
    Route::get('/teams/{id}/edit', [TeamsController::class, 'getTeamsEdit'])->name('get_teams_edit');
    // POST
    Route::post('/teams/add', [TeamsController::class, 'postTeamsAdd'])->name('post_teams_add');
    Route::post('/teams/{id}/edit', [TeamsController::class, 'postTeamsEdit'])->name('post_teams_edit');

    // Module players
    // GET
    Route::get('/players', [PlayersController::class, 'getPlayers'])->name('players');
    Route::get('/players/add', [PlayersController::class, 'getPlayersAdd'])->name('get_players_add');
    Route::get('/players/{id}/edit', [PlayersController::class, 'getPlayersEdit'])->name('get_players_edit');
    // POST
    Route::post('/players/add', [PlayersController::class, 'postPlayersAdd'])->name('post_players_add');
    Route::post('/players/{id}/edit', [PlayersController::class, 'postPlayersEdit'])->name('post_players_edit');

    // ****** Module groups
    // GET
    Route::get('/groups', [GroupsController::class, 'getGroups'])->name('groups');
    Route::get('/groups/add', [GroupsController::class, 'getGroupsAdd'])->name('get_groups_add');
    Route::get('/groups/{id}/edit', [GroupsController::class, 'getGroupsEdit'])->name('get_groups_edit');
    // POST
    Route::post('/groups/add', [GroupsController::class, 'postGroupsAdd'])->name('post_groups_add');
    Route::post('/groups/{id}/edit', [GroupsController::class, 'postGroupsEdit'])->name('post_groups_edit');
    // Module group teams
    // GET
    Route::get('/groups/teams/{id}', [GroupsController::class, 'getGroupTeams'])->name('groups_teams');
    Route::get('/groups/teams/{id}/add', [GroupsController::class, 'getGroupAddTeam'])->name('groups_teams_add');
    // POST
    Route::post('/groups/teams/{id}/add', [GroupsController::class, 'postGroupAddTeam'])->name('group_teams_add_post');
    // Module group matches
    // GET
    Route::get('/groups/{groupId}/matches', [GroupsController::class, 'viewGroupMatches'])->name('view_group_matches');
    Route::get('/matches/{id}/edit', [GroupsController::class, 'getMatchesEdit'])->name('get_matches_edit');
    // POST
    Route::post('/groups/{groupId}/matches', [GroupsController::class, 'addGroupMatch'])->name('add_group_match');
    Route::post('/matches/{id}/edit', [GroupsController::class, 'postMatchesEdit'])->name('post_matches_edit');
    // Module details
    // GET
    Route::get('/matches/{match}/goals', [GroupsController::class, 'showGoals'])->name('show_goals');
    Route::get('/matches/{match}/cards', [GroupsController::class, 'showCards'])->name('show_cards');
    // POST
    Route::post('/matches/{match}/goals', [GroupsController::class, 'storeGoal'])->name('add_goal');
    Route::post('/matches/{match}/cards', [GroupsController::class, 'storeCard'])->name('add_card');
    Route::post('/admin/cards/{cardId}/toggle-state', [GroupsController::class, 'toggleState'])->name('cards.toggle-state');



});
?>
