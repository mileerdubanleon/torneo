<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Multimedia;
use App\Models\Carrusel;
use App\Models\Tournament;

class HomeController extends Controller
{
    //view index
    public function index(){
        $tournaments = Tournament::all();
        return view('public.home', compact('tournaments'));
    }

    // View tournament teams
    public function viewTournamentTeams($id)
    {
        $tournament = Tournament::with(['groups' => function ($query) {
            $query->with('groupDetails.team');
        }])->findOrFail($id);

        $topScorers = $tournament->topScorers();

        return view('public.tournament_team', compact('tournament', 'topScorers'));
    }

}
