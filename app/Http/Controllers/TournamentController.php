<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tournament;


class TournamentController extends Controller
{
    // Mostrar todos los torneos para la vista pública
    public function getTournaments() {
        $tournaments = Tournament::all();
        return view('public.tournament', compact('tournaments'));
    }
}
