<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Player;
use App\Models\Team;
use Validator, Str, Config, Image, Auth;

class PlayersController extends Controller
{
    // construct
    public function __Construct(){
        $this->middleware('auth');
        $this->middleware('isadmin');
    }

    // player view //
    public function getPlayers(){
        // Verificar si el usuario tiene permisos para ver
        if (!kvfj(Auth::user()->permissions, 'players')) {
            abort(403, 'No tienes permisos para ver los jugadores.');
        }

        // Obtener los jugadores desde la base de datos
        $players = Player::with('team')->get(); // Asegúrate de importar el modelo Player al principio del controlador

        // Pasar los jugadores a la vista
        return view('admin.players.home', ['players' => $players]);
    }

    // player add view get //
    public function getPlayersAdd() {
        // Verificar si el usuario tiene permisos para agregar
        if (!kvfj(Auth::user()->permissions, 'players_add')) {
            abort(403, 'No tienes permisos para agregar los jugadores.');
        }
        
        // Obtener todos los equipos
        $teams = Team::all();
        
        // Pasar los equipos a la vista
        return view('admin.players.add', compact('teams'));
    }

    // post add player
    public function postPlayersAdd(Request $request){
        // Reglas de validación
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'team_id' => 'required|exists:teams,id',
            'role' => 'required',
            'document_type' => 'required|integer',
            'identification' => 'required|integer|unique:players,identification',
            'dorsal' => 'required|integer',
            'phone' => 'required',
            'status' => 'required|integer'
        ];

        // Mensajes de error personalizados
        $messages = [
            'first_name.required' => 'El nombre es requerido',
            'last_name.required' => 'El apellido es requerido',
            'team_id.required' => 'El equipo es requerido',
            'team_id.exists' => 'El equipo seleccionado no es válido',
            'role.required' => 'El rol es requerido',
            'document_type.required' => 'El tipo de documento es requerido',
            'document_type.integer' => 'El tipo de documento debe ser un valor numérico',
            'identification.required' => 'La identificación es requerida',
            'identification.integer' => 'La identificación debe ser un valor numérico',
            'identification.unique' => 'La identificación ya está en uso',
            'dorsal.required' => 'El dorsal es requerido',
            'dorsal.integer' => 'El dorsal debe ser un valor numérico',
            'phone.required' => 'El teléfono es requerido',
            'status.required' => 'El estado es requerido',
            'status.integer' => 'El estado debe ser un valor numérico'
        ];

        // Validar los datos del formulario
        $validator = Validator::make($request->all(), $rules, $messages);

        // Si la validación falla, redireccionar de nuevo al formulario con los errores
        if ($validator->fails()) {
            return back()->withErrors($validator)->with('message', 'Se ha producido un error')->with('typealert', 'danger')->withInput();
        }

        // Crear una instancia de Player y asignar los valores
        $player = new Player;
        $player->first_name = e($request->input('first_name'));
        $player->last_name = e($request->input('last_name'));
        $player->team_id = $request->input('team_id');
        $player->role = e($request->input('role'));
        $player->document_type = $request->input('document_type');
        $player->identification = $request->input('identification');
        $player->dorsal = $request->input('dorsal');
        $player->phone = $request->input('phone');
        $player->status = $request->input('status');

        // Guardar el jugador en la base de datos
        if ($player->save()) {
            return redirect('/admin/players')->with('message', 'Jugador guardado con éxito')->with('typealert', 'success');
        } else {
            return back()->with('message', 'Error al guardar el jugador')->with('typealert', 'danger')->withInput();
        }
    }
}
