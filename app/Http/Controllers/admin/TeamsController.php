<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Team;
use App\Models\Tournament;
use Validator, Str, Config, Image, Auth;

class TeamsController extends Controller
{
    //
    // construct
    public function __Construct(){
        $this->middleware('auth');
        // $this->middleware('user.status');
        // $this->middleware('user.permissions');
        $this->middleware('isadmin');
    }
    // team view //
    public function getTeams(){
        // Verificar si el usuario tiene permisos para editar
        if (!kvfj(Auth::user()->permissions, 'teams')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para ver los equipos.');
        }

        // Obtener los torneos desde la base de datos
        $teams = Team::all(); // Asegúrate de importar el modelo Tournament al principio del controlador

        // Pasar los torneos a la vista
        return view('admin.teams.home', ['teams' => $teams]);
    }
    // team add view get //
    public function getTeamsAdd() {
        // Verificar si el usuario tiene permisos para editar
        if (!kvfj(Auth::user()->permissions, 'teams_add')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para agregar los equipos.');
        }
    
        // Obtener todos los torneos
        $tournaments = Tournament::all();
    
        // Pasar los torneos a la vista
        return view('admin.teams.add', compact('tournaments'));
    }
    // post add team
    public function postTeamsAdd(Request $request){
        // Reglas de validación
        $rules = [
            'team_name' => 'required',
            'uniform' => 'required',
            'description' => 'nullable',
            'logo' => 'image|mimes:jpeg,png,jpg,gif|max:2048', // Reglas para la imagen
            'status' => 'required|integer',
            'tournament_id' => 'nullable|exists:tournaments,id'
        ];

        // Mensajes de error personalizados
        $messages = [
            'team_name.required' => 'El nombre del equipo es requerido',
            'uniform.required' => 'El uniforme del equipo es requerido',
            'logo.image' => 'El archivo seleccionado debe ser una imagen',
            'logo.mimes' => 'El archivo debe ser de tipo jpeg, png, jpg o gif',
            'logo.max' => 'El tamaño máximo permitido para la imagen es de 2MB',
            'status.required' => 'El estado del equipo es requerido',
            'status.integer' => 'El estado del equipo debe ser un valor numérico',
            'tournament_id.exists' => 'El torneo seleccionado no es válido'
        ];

        // Validar los datos del formulario
        $validator = Validator::make($request->all(), $rules, $messages);

        // Si la validación falla, redireccionar de nuevo al formulario con los errores
        if ($validator->fails()) {
            return back()->withErrors($validator)->with('message', 'Se ha producido un error')->with('typealert', 'danger')->withInput();
        }

        // Crear una instancia de Team y asignar los valores
        $team = new Team;
        $team->team_name = e($request->input('team_name'));
        $team->uniform = e($request->input('uniform'));
        $team->description = e($request->input('description'));
        $team->status = $request->input('status');
        $team->tournament_id = $request->input('tournament_id');

        // Si se cargó una imagen, guardarla
        if ($request->hasFile('logo')) {
            $image = $request->file('logo');
            $imageName = Str::slug($request->input('team_name')) . '.' . $image->getClientOriginalExtension();
            $path = $image->storeAs('', $imageName, 'uploads_team_logo');
            $team->logo = $path;
        }

        // Guardar el equipo en la base de datos
        if ($team->save()) {
            return redirect('/admin/teams')->with('message', 'Equipo guardado con éxito')->with('typealert', 'success');
        } else {
            return back()->with('message', 'Error al guardar el equipo')->with('typealert', 'danger')->withInput();
        }
    }
}
