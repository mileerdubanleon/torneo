<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tournament;
use App\Models\Group;


use Validator, Str, Config, Image, Auth;

class TournamentsController extends Controller
{
    // construct
    public function __Construct(){
        $this->middleware('auth');
        // $this->middleware('user.status');
        // $this->middleware('user.permissions');
        $this->middleware('isadmin');
    }
    // torneos view //
    public function getTournaments(){
        // Verificar si el usuario tiene permisos para editar
        if (!kvfj(Auth::user()->permissions, 'tournaments')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para ver los torneos.');
        }

        // Obtener los torneos desde la base de datos
        $tournaments = Tournament::all(); // Asegúrate de importar el modelo Tournament al principio del controlador

        // Pasar los torneos a la vista
        return view('admin.tournament.home', ['tournaments' => $tournaments]);
    }
    // torneo add view get //
    public function getTournamentsAdd(){
        // Verificar si el usuario tiene permisos para editar
        if (!kvfj(Auth::user()->permissions, 'tournaments_add')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para ver los torneos.');
        }
    	return view('admin.tournament.add');
    }
    // post add torneo
    public function postTournamentsAdd(Request $request){
        // Verificar si el usuario tiene permisos para editar
        if (!kvfj(Auth::user()->permissions, 'tournaments_add')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para agregar los torneos.');
        }
        // Reglas de validación para los campos del formulario
        $rules = [
            'name' => 'required',
            'no_teams' => 'required|integer|min:1',
            'no_max_players_per_team' => 'required|integer|min:1',
            'no_max_groups' => 'required|integer|min:1',
            'place' => 'required|string|max:255',
            'description' => 'nullable|string',
            'status' => 'required|string|in:activo,inactivo',
            'logo' => 'image|mimes:jpeg,png,jpg,gif|max:2048', // Reglas para la imagen
        ];

        // Mensajes de error personalizados para las reglas de validación
        $messages = [
            'name.required' => 'El nombre del torneo es requerido',
            'no_teams.required' => 'El número de equipos es requerido',
            'no_teams.integer' => 'El número de equipos debe ser un número entero',
            'no_max_players_per_team.required' => 'El número máximo de jugadores por equipo es requerido',
            'no_max_groups.required' => 'El número máximo de grupos es requerido',
            'place.required' => 'El lugar del torneo es requerido',
            'status.required' => 'El estado del torneo es requerido',
            'logo.image' => 'El archivo seleccionado debe ser una imagen',
            'logo.mimes' => 'El archivo debe ser de tipo jpeg, png, jpg o gif',
            'logo.max' => 'El tamaño máximo permitido para la imagen es de 2MB',
        ];

        // Validar los datos del formulario
        $validator = Validator::make($request->all(), $rules, $messages);

        // Si la validación falla, redireccionar de nuevo al formulario con los errores
        if ($validator->fails()) {
            return back()->withErrors($validator)->with('message', 'Se ha producido un error')->with('typealert', 'danger')->withInput();
        }

        // Crear una instancia de Tournament y asignar los valores
        $tournament = new Tournament;
        $tournament->name = e($request->input('name'));
        $tournament->no_teams = $request->input('no_teams');
        $tournament->no_max_players_per_team = $request->input('no_max_players_per_team');
        $tournament->no_max_groups = $request->input('no_max_groups');
        $tournament->place = e($request->input('place'));
        $tournament->description = e($request->input('description'));
        $tournament->status = $request->input('status') == 'activo' ? 1 : 0;

        // Si se cargó una imagen, guardarla
        if ($request->hasFile('logo')) {
            $logo = $request->file('logo');
            $logoName = Str::slug($request->input('name')) . '.' . $logo->getClientOriginalExtension();
            $path = $logo->storeAs('', $logoName, 'uploads_torneo_logo');
            $tournament->logo = $path;
        }

        // Guardar el torneo en la base de datos
        if ($tournament->save()) {
            return redirect('/admin/tournaments')->with('message', 'Guardado con éxito.')->with('typealert', 'success');
        } else {
            return back()->with('message', 'Error al guardar el torneo')->with('typealert', 'danger')->withInput();
        }
    }

    // ******************Group Teams*********
    // View tournament teams
    public function viewTournamentTeams($id) {
        
        // Obtener el torneo con sus grupos y los detalles de los equipos en esos grupos
        $tournament = Tournament::with(['groups.groupDetails.team'])->findOrFail($id);
        
        return view('admin.tournament.teamsGroup', compact('tournament'));
    }

    
}
