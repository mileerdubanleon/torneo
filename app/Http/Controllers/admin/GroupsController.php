<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Group;
use App\Models\Team;
use App\Models\Tournament;
use App\Models\GroupDetail;
use App\Models\Matchfut;
use App\Models\Player;
use App\Models\Goal;
use App\Models\Card;
use Validator, Str, Config, Auth;

class GroupsController extends Controller
{
    // function de session
    public function __Construct(){
        $this->middleware('auth');
        $this->middleware('isadmin');
    }
    // Mostrar todos los grupos
    public function getGroups() {
        // Verificar si el usuario tiene permisos para ver grupos
        if (!kvfj(Auth::user()->permissions, 'groups')) {
            abort(403, 'No tienes permisos para ver los grupos.');
        }

        // Obtener los grupos desde la base de datos
        $groups = Group::with('tournament')->get();

        // Pasar los grupos a la vista
        return view('admin.groups.home', compact('groups'));
    }

    // Mostrar formulario para agregar un nuevo grupo
    public function getGroupsAdd() {
        // Verificar si el usuario tiene permisos para agregar grupos
        if (!kvfj(Auth::user()->permissions, 'groups_add')) {
            abort(403, 'No tienes permisos para agregar grupos.');
        }

        // Obtener todos los torneos
        $tournaments = Tournament::all();

        // Pasar los torneos a la vista
        return view('admin.groups.add', compact('tournaments'));
    }

    // Manejar la solicitud POST para agregar un nuevo grupo
    public function postGroupsAdd(Request $request) {
        // Reglas de validación
        $rules = [
            'name' => 'required|string|max:255',
            'team_max' => 'required|integer|max:255',
            'tournament_id' => 'required|exists:tournaments,id',
        ];

        // Mensajes de error personalizados
        $messages = [
            'name.required' => 'El nombre del grupo es requerido',
            'name.string' => 'El nombre del grupo debe ser una cadena de texto',
            'name.max' => 'El nombre del grupo no puede exceder los 255 caracteres',
            'team_max.required' => 'El equipo es requerido',
            'team_max.string' => 'El nombre del equipo debe ser una cadena de texto',
            'team_max.max' => 'El nombre del equipo no puede exceder los 255 caracteres',
            'tournament_id.required' => 'El torneo es requerido',
            'tournament_id.exists' => 'El torneo seleccionado no es válido',
        ];

        // Validar los datos del formulario
        $validator = Validator::make($request->all(), $rules, $messages);

        // Si la validación falla, redireccionar de nuevo al formulario con los errores
        if ($validator->fails()) {
            return back()->withErrors($validator)->with('message', 'Se ha producido un error')->with('typealert', 'danger')->withInput();
        }

        // Crear una instancia de Group y asignar los valores
        $group = new Group;
        $group->name = e($request->input('name'));
        $group->no_teams = e($request->input('team_max'));
        $group->tournament_id = $request->input('tournament_id');

        // Guardar el grupo en la base de datos
        if ($group->save()) {
            return redirect()->route('groups')->with('message', 'Grupo creado con éxito')->with('typealert', 'success');
        } else {
            return back()->with('message', 'Error al guardar el grupo')->with('typealert', 'danger')->withInput();
        }
    }

    // Mostrar formulario para editar un grupo existente
    public function getGroupsEdit($id) {
        // Verificar si el usuario tiene permisos para editar grupos
        if (!kvfj(Auth::user()->permissions, 'groups_edit')) {
            abort(403, 'No tienes permisos para editar grupos.');
        }

        // Obtener el grupo por ID
        $group = Group::findOrFail($id);
        $tournaments = Tournament::all();

        // Pasar el grupo y los torneos a la vista
        return view('admin.groups.edit', compact('group', 'tournaments'));
    }

    // Manejar la solicitud POST para editar un grupo existente
    public function postGroupsEdit(Request $request, $id) {
        // Reglas de validación
        $rules = [
            'name' => 'required|string|max:255',
            'tournament_id' => 'required|exists:tournaments,id',
        ];

        // Mensajes de error personalizados
        $messages = [
            'name.required' => 'El nombre del grupo es requerido',
            'name.string' => 'El nombre del grupo debe ser una cadena de texto',
            'name.max' => 'El nombre del grupo no puede exceder los 255 caracteres',
            'tournament_id.required' => 'El torneo es requerido',
            'tournament_id.exists' => 'El torneo seleccionado no es válido',
        ];

        // Validar los datos del formulario
        $validator = Validator::make($request->all(), $rules, $messages);

        // Si la validación falla, redireccionar de nuevo al formulario con los errores
        if ($validator->fails()) {
            return back()->withErrors($validator)->with('message', 'Se ha producido un error')->with('typealert', 'danger')->withInput();
        }

        // Obtener el grupo por ID y actualizar los valores
        $group = Group::findOrFail($id);
        $group->name = e($request->input('name'));
        $group->tournament_id = $request->input('tournament_id');

        // Guardar el grupo en la base de datos
        if ($group->save()) {
            return redirect()->route('groups')->with('message', 'Grupo actualizado con éxito')->with('typealert', 'success');
        } else {
            return back()->with('message', 'Error al actualizar el grupo')->with('typealert', 'danger')->withInput();
        }
    }

    // ******************  team group details

    // Mostrar los equipos asignados a un grupo específico
    public function getGroupTeams($id) {
        // Verificar si el usuario tiene permisos para ver equipos del grupo
        if (!kvfj(Auth::user()->permissions, 'groups_teams')) {
            abort(403, 'No tienes permisos para ver los equipos del grupo.');
        }

        // Obtener el grupo y sus equipos
        $group = Group::with('details.team')->findOrFail($id);

        // Pasar los equipos a la vista
        return view('admin.groups.groupTeams', compact('group'));
    }

    // view add group team
    public function getGroupAddTeam($id) {
        // Verificar si el usuario tiene permisos para agregar equipos al grupo
        if (!kvfj(Auth::user()->permissions, 'groups_teams_add')) {
            abort(403, 'No tienes permisos para agregar equipos al grupo.');
        }
    
        // Obtener el grupo por ID
        $group = Group::findOrFail($id);
    
        // Obtener todos los equipos
        $teams = Team::all()->pluck('team_name', 'id');
    
        // Pasar el grupo y los equipos a la vista
        return view('admin.groups.groupTeamsAdd', compact('group', 'teams'));
    }

    // Manejar la solicitud POST para agregar un equipo a un grupo
    public function postGroupAddTeam(Request $request, $id) {
        // Verificar si el usuario tiene permisos para agregar equipos al grupo
        // if (!kvfj(Auth::user()->permissions, 'group_add_team')) {
        //     abort(403, 'No tienes permisos para agregar equipos al grupo.');
        // }
    
        // Reglas de validación
        $rules = [
            'team_id' => 'required|exists:teams,id',
        ];
    
        // Mensajes de error personalizados
        $messages = [
            'team_id.required' => 'El equipo es requerido',
            'team_id.exists' => 'El equipo seleccionado no es válido',
        ];
    
        // Validar los datos del formulario
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // Si la validación falla, redireccionar de nuevo al formulario con los errores
        if ($validator->fails()) {
            return back()->withErrors($validator)->with('message', 'Se ha producido un error')->with('typealert', 'danger')->withInput();
        }
    
        // Verificar si el equipo ya está en cualquier grupo
        $teamInGroup = GroupDetail::where('team_id', $request->input('team_id'))->exists();
        if ($teamInGroup) {
            return back()->with('message', 'El equipo ya está en otro grupo')->with('typealert', 'danger')->withInput();
        }
    
        // Agregar el equipo al grupo
        GroupDetail::create([
            'team_id' => $request->input('team_id'),
            'group_id' => $id,
        ]);
    
        // Redirigir con mensaje de éxito
        return redirect()->route('groups_teams', ['id' => $id])->with('message', 'Equipo agregado con éxito')->with('typealert', 'success');
    }

    // ******************  matches group details
    // Mostrar los partidos de un grupo
    public function viewGroupMatches($groupId){
        $group = Group::with('tournament', 'teams')->findOrFail($groupId);
        $matches = Matchfut::where('group_id', $groupId)->get();
        
        return view('admin.groups.groupMatches', compact('group', 'matches'));
    }

    // Agregar un partido al grupo
    public function addGroupMatch(Request $request, $groupId) {
        $request->validate([
            'name' => 'required|string|max:255',
            'team_1' => 'required|exists:teams,id',
            'team_2' => 'required|exists:teams,id',
            'hour' => 'required',
            'place' => 'required|string|max:255',
            'result_team1' => 'nullable|integer',
            'result_team2' => 'nullable|integer',
        ]);

        // Crear una nueva instancia del modelo Matchfut y asignar los valores
        $match = new Matchfut();
        $match->name = $request->name;
        $match->team_1 = $request->team_1;
        $match->team_2 = $request->team_2;
        $match->hour = $request->hour;
        $match->place = $request->place;
        $match->result_team1 = $request->result_team1; // Asignación explícita del resultado del equipo 1
        $match->result_team2 = $request->result_team2; // Asignación explícita del resultado del equipo 2
        $match->group_id = $groupId;
        $match->status = 0; // Status predeterminado

        // Intentar guardar el partido
        if ($match->save()) {
            return redirect()->route('view_group_matches', $groupId)
                ->with('message', 'Partido creado con éxito')
                ->with('typealert', 'success');
        } else {
            return back()->with('message', 'Error al guardar el partido')
                ->with('typealert', 'danger')
                ->withInput();
        }
    }


    // Mostrar formulario para editar un partido existente
    public function getMatchesEdit($id) {
        $match = Matchfut::findOrFail($id);
        $groupId = $match->group_id;
        $group = Group::with('teams')->findOrFail($groupId);
        $teams = $group->teams->pluck('team_name', 'id');
        $groups = Group::pluck('name', 'id');
        return view('admin.groups.groupMatchesEdit', compact('match', 'teams', 'groups'));
    }

    // Manejar la solicitud POST para editar un partido existente
    public function postMatchesEdit(Request $request, $id) {
        // Definir las reglas de validación
        $rules = [
            'name' => 'required|string|max:255',
            'hour' => 'required',
            'place' => 'required|string|max:255',
            'result_team1' => 'nullable|integer',
            'result_team2' => 'nullable|integer',
        ];
    
        // Definir los mensajes de error personalizados
        $messages = [
            'name.required' => 'El nombre del partido es requerido',
            'hour.required' => 'La hora es requerida',
            'place.required' => 'El lugar es requerido',
        ];
    
        // Realizar la validación de los datos de entrada
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // Verificar si la validación falla
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with('message', 'Se ha producido un error al validar los datos.')
                ->with('typealert', 'danger');
        }
    
        try {
            // Encontrar el partido a editar por su ID
            $match = Matchfut::findOrFail($id);
            // Actualizar los campos del partido con los datos del formulario
            $match->name = $request->input('name');
            $match->hour = $request->input('hour');
            $match->place = $request->input('place');
            $match->result_team1 = $request->input('result_team1', $match->result_team1); // Actualizar el resultado del equipo 1 si está presente
            $match->result_team2 = $request->input('result_team2', $match->result_team2); // Actualizar el resultado del equipo 2 si está presente
    
            // Guardar los cambios en la base de datos
            if ($match->save()) {
                return redirect()->route('view_group_matches', $match->group_id)
                    ->with('message', 'Partido editado con éxito.')
                    ->with('typealert', 'success');
            } else {
                return back()
                    ->withInput()
                    ->with('message', 'Error al actualizar el partido.')
                    ->with('typealert', 'danger');
            }
        } catch (\Exception $e) {
            return back()
                ->withInput()
                ->with('message', 'Error: ' . $e->getMessage())
                ->with('typealert', 'danger');
        }
    }

    // ******************  matches players details
    // Mostrar los players 
    public function showGoals($matchId){
        // Obtener el partido por su ID
        $match = Matchfut::findOrFail($matchId);

        // Obtener los jugadores por equipo
        $team1Players = Player::where('team_id', $match->team_1)->get();
        $team2Players = Player::where('team_id', $match->team_2)->get();

        // Organizar los jugadores en un array asociativo por ID de equipo
        $playersByTeam = [
            $match->team_1 => $team1Players,
            $match->team_2 => $team2Players,
        ];

        return view('admin.matches.goals', [
            'match' => $match,
            'playersByTeam' => $playersByTeam,
        ]);
    }

    // Mostrar los players
    public function showCards($matchId){
        $match = Matchfut::with(['cards.team', 'cards.player', 'teamLocal.players', 'teamVisitor.players'])
                         ->findOrFail($matchId);
    
        // Obtener los jugadores por equipo
        $team1Players = Player::where('team_id', $match->team_1)->get();
        $team2Players = Player::where('team_id', $match->team_2)->get();
    
        // Organizar los jugadores en un array asociativo por ID de equipo
        $playersByTeam = [
            $match->team_1 => $team1Players,
            $match->team_2 => $team2Players,
        ];
    
        return view('admin.matches.card', [
            'match' => $match,
            'playersByTeam' => $playersByTeam,
        ]);
    }

    public function storeGoal(Request $request, $matchId) {
        $request->validate([
            'team_id' => 'required',
            'player_id' => 'required',
            'minute' => 'required|integer'
        ]);

        Goal::create([
            'match_id' => $matchId,
            'team_id' => $request->team_id,
            'player_id' => $request->player_id,
            'minute' => $request->minute
        ]);

        return redirect()->route('show_goals', $matchId)->with('success', 'Gol agregado exitosamente.');
    }

    public function storeCard(Request $request, $matchId){
        $request->validate([
            'team_id' => 'required',
            'player_id' => 'required',
            'type' => 'required',
            'minute' => 'required|integer',
            'state' => 'sometimes|integer' // Validar el estado
        ]);

        Card::create([
            'match_id' => $matchId,
            'team_id' => $request->team_id,
            'player_id' => $request->player_id,
            'type' => $request->type,
            'minute' => $request->minute,
            'state' => $request->state ?? 0 // Establecer el estado predeterminado si no está presente
        ]);

        return redirect()->route('show_cards', $matchId)->with('success', 'Tarjeta agregada exitosamente.');
    }

    public function toggleState($cardId){
        Log::info('Intentando cambiar el estado para la tarjeta con ID: ' . $cardId);

        try {
            $card = Card::findOrFail($cardId);
            $newState = $card->state == 1 ? 0 : 1;
            $card->state = $newState;
            $card->save();

            Log::info('Estado cambiado correctamente. Nuevo estado: ' . $newState);

            return response()->json(['success' => true, 'newState' => $newState]);
        } catch (\Exception $e) {
            Log::error('Error al cambiar el estado: ' . $e->getMessage());

            return response()->json(['success' => false, 'error' => 'Error al cambiar el estado.']);
        }
}

}
