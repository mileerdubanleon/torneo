<?php

// Key value From Json
function kvfj($json, $key){
    if($json == null):
        return null;
    else:
        $json = $json;
        $json = json_decode($json, true);
        if(array_key_exists($key, $json)):
            return $json[$key];
        else:
            return null;
        endif;
    endif;    
}

function getModulesArray(){
    $a = [
        '0' => 'Productos',
        '1' => 'Blog'
    ];
        return $a;
}

function getRoleUserArray($mode, $id){
    $roles = ['0' => 'Usuario normal', '1' => 'Administrador'];
    if(!is_null($mode)):
                return $roles;
        else:
                return $roles[$id];
    endif;
}

function getUserStatusArray($mode, $id){
    $status = ['0' => 'Registrado', '1' => 'Verificado', '100' => 'Baneado'];
    if(!is_null($mode)):
                return $status;
        else:
                return $status[$id];
    endif;
}

function user_permissions(){
    $p = [
        'dashboard' => [
            'icon' => '<i class="fas fa-home"></i>',
            'title' => 'Modulo Dashboard',
            'keys' => [
                'dashboard' => 'Puede ver el dashboard.'
            ]
        ],

        'users' => [
            'icon' => '<i class="fas fa-user-friends"></i>',
            'title' => 'Modulo Usuarios',
            'keys' => [
                'user_list' => 'Puede ver el listado de usuarios.',
                'user_edit' => 'Puede editar usuarios.',
                'user_banned' => 'Puede banear usuarios.',
                'user_permissions_get' => 'Puede administrar permisos de usuarios.',
                'user_permissions' => 'Puede cambiar permisos de usuarios.',
            ]
        ],

        'tournaments' => [
            'icon' => '<i class="fas fa-folder-open"></i>',
            'title' => 'Modulo Torneo',
            'keys' => [
                'tournaments' => 'Puede ver el listado de los torneos.',
                'tournaments_add' => 'Puede agregar torneos.',
                'tournaments_edit' => 'Puede editar los torneos.',
                'tournaments_delete' => 'Puede eliminar los torneos.',
            ]
        ],

        'teams' => [
            'icon' => '<i class="fas fa-users"></i>',
            'title' => 'Modulo Equipos',
            'keys' => [
                'teams' => 'Puede ver el listado de equipos.',
                'teams_add' => 'Puede agregar equipos.',
                'teams_edit' => 'Puede editar equipos.',
                'teams_delete' => 'Puede eliminar equipos.',
            ]
        ],

        'players' => [
            'icon' => '<i class="fas fa-user"></i>',
            'title' => 'Modulo Jugadores',
            'keys' => [
                'players' => 'Puede ver el listado de jugadores.',
                'players_add' => 'Puede agregar jugadores.',
                'players_edit' => 'Puede editar jugadores.',
                'players_delete' => 'Puede eliminar jugadores.',
            ]
        ],

        'matches' => [
            'icon' => '<i class="fas fa-futbol"></i>',
            'title' => 'Modulo Partidos',
            'keys' => [
                'matches' => 'Puede ver el listado de partidos.',
                'matches_add' => 'Puede agregar partidos.',
                'matches_edit' => 'Puede editar partidos.',
                'matches_delete' => 'Puede eliminar partidos.',
            ]
        ],

        'match_details' => [
            'icon' => '<i class="fas fa-list-alt"></i>',
            'title' => 'Modulo Detalle de Partidos',
            'keys' => [
                'match_details' => 'Puede ver el detalle de partidos.',
                'match_details_add' => 'Puede agregar detalles de partidos.',
                'match_details_edit' => 'Puede editar detalles de partidos.',
                'match_details_delete' => 'Puede eliminar detalles de partidos.',
            ]
        ],

        'goals' => [
            'icon' => '<i class="fas fa-futbol"></i>',
            'title' => 'Modulo Goles',
            'keys' => [
                'goals' => 'Puede ver el listado de goles.',
                'goals_add' => 'Puede agregar goles.',
                'goals_edit' => 'Puede editar goles.',
                'goals_delete' => 'Puede eliminar goles.',
            ]
        ],

        'groups' => [
            'icon' => '<i class="fas fa-layer-group"></i>',
            'title' => 'Modulo Grupos',
            'keys' => [
                'groups' => 'Puede ver el listado de grupos.',
                'groups_add' => 'Puede agregar grupos.',
                'groups_edit' => 'Puede editar grupos.',
                'groups_delete' => 'Puede eliminar grupos.',
                'groups_teams' => 'Puede ver los equipo del grupo.',
                'groups_teams_add' => 'Puede agregar los equipo al grupo.',
            ]
        ],

    ];

    return $p;
}


function getUserYears(){
    $ya = date('Y');
    $ym = $ya - 18;
    $yo = $ym - 62;

    return [$ym,$yo];
}

function getMonths($mode, $key){
    $m = [
        '01' => 'Enero',
        '02' => 'Febrero',
        '03' => 'Marzo',
        '04' => 'Abril',
        '05' => 'Mayo',
        '06' => 'Junio',
        '07' => 'Julio',
        '08' => 'Agosto',
        '09' => 'Septiembre',
        '10' => 'Octubre',
        '11' => 'Noviembre',
        '12' => 'Diciembre'
    ];
    if($mode == "list"){
        return $m;
    }else{
        return $m[$key];
    }
}

function getStatusClass($status)
{
    switch ($status) {
        case 0:
            return 'bg-light text-dark'; // Por Jugar
        case 1:
            return 'bg-success text-white'; // Jugando
        case 2:
            return 'bg-danger text-white'; // Finalizado
        default:
            return '';
    }
}

function getStatusText($status)
{
    switch ($status) {
        case 0:
            return 'Por Jugar';
        case 1:
            return 'Jugando';
        case 2:
            return 'Finalizado';
        default:
            return '';
    }
}

?>