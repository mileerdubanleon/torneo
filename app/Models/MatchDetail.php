<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MatchDetail extends Model
{
    use HasFactory;

    protected $table = 'match_details';

    protected $fillable = [
        'match_id',
        'player_id',
        'goals',
    ];

    public function match()
    {
        return $this->belongsTo(Matchfut::class);
    }

    public function player()
    {
        return $this->belongsTo(Player::class);
    }
}
