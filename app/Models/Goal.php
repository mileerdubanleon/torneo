<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Goal extends Model
{
    use HasFactory;

    protected $dates = ['deleted_at'];
    protected $table = 'goals';
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['match_id', 'team_id', 'player_id', 'minute'];

    public function match()
    {
        return $this->belongsTo(Matchfut::class, 'match_id');
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    public function player()
    {
        return $this->belongsTo(Player::class, 'player_id');
    }


}
