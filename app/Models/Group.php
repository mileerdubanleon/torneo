<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'groups';
    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = ['name', 'no_teams', 'tournament_id'];
    
    public function tournament()
    {
        return $this->belongsTo(Tournament::class);
    }

    public function groupDetails()
    {
        return $this->hasMany(GroupDetail::class);
    }
    
    public function details(){
        return $this->hasMany(GroupDetail::class);
    }

    public function teams(){
        return $this->hasManyThrough(Team::class, GroupDetail::class, 'group_id', 'id', 'id', 'team_id');
    }

}
