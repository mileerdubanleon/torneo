<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupDetail extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'details_groups';
    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = [
        'team_id', // Aquí agregamos el team_id
        'group_id', // Asegúrate de incluir también group_id si es necesario
        // Otros campos que permitan asignación masiva
    ];

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    // Definir la relación con los partidos
    public function matches()
    {
        return Matchfut::where('team_1', $this->team_id)
                        ->orWhere('team_2', $this->team_id)
                        ->get();
    }
    
}
