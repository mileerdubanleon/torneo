<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'teams';
    protected $hidden = ['created_at', 'updated_at'];

    // Define the relationship with Tournament
    public function tournament(){
        return $this->belongsTo(Tournament::class);
    }

    public function groups(){
        return $this->belongsToMany(Group::class, 'group_team');
    }

    public function groupDetails(){
        return $this->hasMany(GroupDetail::class);
    }

    public function players()
    {
        return $this->hasMany(Player::class);
    }
    

}
