<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tournament extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ['delete_at'];
    protected $table = 'tournaments';
    protected $hidden = ['created_at', 'updated_at'];

    public function tournament()
    {
        return $this->belongsTo(Tournament::class);
    }

    public function groups() {
        return $this->hasMany(Group::class);
    }

    // Método para obtener los máximos goleadores del torneo
    public function topScorers()
{
    return Player::select('players.id', 'players.first_name', 'players.last_name', \DB::raw('COUNT(goals.id) as goals'))
        ->join('goals', 'players.id', '=', 'goals.player_id')
        ->join('matches', 'goals.match_id', '=', 'matches.id')
        ->join('groups', 'matches.group_id', '=', 'groups.id')
        ->where('groups.tournament_id', $this->id)
        ->groupBy('players.id', 'players.first_name', 'players.last_name')
        ->orderByDesc('goals')
        ->get();
}
}
