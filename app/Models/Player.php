<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Player extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'players';
    protected $hidden = ['created_at', 'updated_at'];

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    public function cards()
    {
        return $this->hasMany(Card::class, 'player_id');
    }

    public function goals()
    {
        return $this->hasMany(Goal::class);
    }

}
