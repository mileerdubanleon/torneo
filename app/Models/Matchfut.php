<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Matchfut extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'matches';

    protected $fillable = [
        'name', 'team_1', 'team_2', 'hour', 'place', 'status', 'result_team1', 'result_team2', 'result_win', 'result_lose', 'group_id'
    ];

    public function tournament()
    {
        return $this->belongsTo(Tournament::class);
    }

    public function teamLocal()
    {
        return $this->belongsTo(Team::class, 'team_1');
    }

    public function teamVisitor()
    {
        return $this->belongsTo(Team::class, 'team_2');
    }

    public function matchDetails()
    {
        return $this->hasMany(MatchDetail::class);
    }

    public function goals()
    {
        return $this->hasMany(Goal::class, 'match_id');
    }

    public function cards()
    {
        return $this->hasMany(Card::class, 'match_id');
    }
    
}
