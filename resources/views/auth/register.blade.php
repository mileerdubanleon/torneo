@extends('auth.master')
@section('title', 'Registro')
@section('content')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
@if(Session::has('message') || $errors->has('email'))
    <div class="position-fixed d-flex justify-content-center text-align-center w-100">
        <div class="w-100 alert alert-{{ Session::get('typealert') }}" style="display:none;">
            @if(Session::has('message'))
                <span>
                    Ha ocurrido un error
                </span>
            @endif

            <div class="">
                @if($errors->has('name'))
                    {{ $errors->first('name') }}
                @endif
            </div>

            <div class="">
                @if($errors->has('lastname'))
                    {{ $errors->first('lastname') }}
                @endif
            </div>

            <div class="">
                @if($errors->has('email'))
                    {{ $errors->first('email') }}
                @endif
            </div>

            @if($errors->has('password'))
                {{ $errors->first('password') }}
            @endif

            <script>
                $('.alert').slideDown();
                setTimeout(function(){ $('.alert').slideUp(); }, 10000)
            </script>
        </div>
    </div>
@endif

{{-- register --}}
<div class="w-full d-flex justify-content-center align-items-center" style="height: 100vh;">
    
    <div class="container" style="max-width: 500px; height: 100vh;">
        <h3 class="container-fluid mt-1 pt-4" style="font-weight: 400;">Introduce tu dirección de correo electrónico y datos personales para unirte.</h3>

        <form method="POST" action="{{ url('/register') }}">
            @csrf

            <input class="form my-4 d-flex container input-connect" type="text" name="name" placeholder="Nombres" >

            <input class="form my-4 d-flex container input-connect" type="text" name="lastname" placeholder="Apellidos" >
    
            <input class="form my-4 d-flex container input-connect" type="email" name="email" placeholder="Correo electrónico*" >

            <input class="form mt-4 d-flex container input-connect" type="password" name="password" placeholder="Contraseña" >

            <input class="form mt-4 d-flex container input-connect" type="password" name="cpassword" placeholder="Repita su contraseña" >

            {{-- Google login button --}}
            <div class="text-center my-4 d-flex justify-content-center">
                {{-- btn google --}}
                <a href="{{ url('/auth/google') }}" 
                    class="btn btn-light mx-2 d-flex align-items-center justify-content-center" 
                    style="background-color: #fff; color: #000; text-decoration: none; width: 80px; height: 50px;">
                    {{-- logo google --}}
                    <svg xmlns="http://www.w3.org/2000/svg" class="p-1" x="0px" y="0px" width="100" height="100" viewBox="0 0 48 50">
                        <path fill="#FFC107" d="M43.611,20.083H42V20H24v8h11.303c-1.649,4.657-6.08,8-11.303,8c-6.627,0-12-5.373-12-12c0-6.627,5.373-12,12-12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657C34.046,6.053,29.268,4,24,4C12.955,4,4,12.955,4,24c0,11.045,8.955,20,20,20c11.045,0,20-8.955,20-20C44,22.659,43.862,21.35,43.611,20.083z"></path><path fill="#FF3D00" d="M6.306,14.691l6.571,4.819C14.655,15.108,18.961,12,24,12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657C34.046,6.053,29.268,4,24,4C16.318,4,9.656,8.337,6.306,14.691z"></path><path fill="#4CAF50" d="M24,44c5.166,0,9.86-1.977,13.409-5.192l-6.19-5.238C29.211,35.091,26.715,36,24,36c-5.202,0-9.619-3.317-11.283-7.946l-6.522,5.025C9.505,39.556,16.227,44,24,44z"></path><path fill="#1976D2" d="M43.611,20.083H42V20H24v8h11.303c-0.792,2.237-2.231,4.166-4.087,5.571c0.001-0.001,0.002-0.001,0.003-0.002l6.19,5.238C36.971,39.205,44,34,44,24C44,22.659,43.862,21.35,43.611,20.083z"></path>
                    </svg>
                </a>
                {{-- btn facebook --}}
                <a href="{{ url('/auth/google') }}" 
                    class="btn btn-light d-flex align-items-center justify-content-center" 
                    style="background-color: #fff; color: #000; text-decoration: none; width: 80px; height: 50px;">
                    {{-- logo facebook --}}
                    <img class="w-75" src="https://cdn.icon-icons.com/icons2/1826/PNG/512/4202107facebookfblogosocialsocialmedia-115710_115591.png" alt="">
                </a>
            </div>

            <div class="container text-center my-2" style="font-size: 12px;">
                Al continuar, acepto la Política de privacidad y los Términos de uso de la Tienda.
            </div>

            <div class="my-3 mx-4 d-flex position-links">
                <a href="{{ url('/login') }}" class="col text-center font-weight-bold" style="color: #000;font-size: 13px;">
                    Inicia sesión
                </a>
                <a href="#" class="col font-weight-bold text-center" style="color: #000; font-size: 13px;">
                    Olvide mi contraseña
                </a>
            </div>

            <div class="container pb-5">
                <button type="submit" class="btn btn-outline-dark form-control">Continuar</button>
            </div>
        </form>

    </div>
</div>
@endsection




    
    