<div class="col-md-12 p-4">
    <!-- Small Cards for Matches -->
    <div class="row mt-4">
        <div class="col-md-3 mb-3">
            <div class="card rounded-md">
                <div class="card-body">
                    <div class="d-flex align-items-center justify-content-between">
                        <span class="text-truncate">Equipo A</span>
                        <span class="mx-2 badge bg-secondary text-white">vs</span>
                        <span class="text-truncate">Equipo B</span>
                    </div>
                    <div class="text-center mt-2">
                        <small class="text-muted">10:00 AM</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <div class="card rounded-md">
                <div class="card-body">
                    <span class="bg-success text-white rounded-md d-flex justify-content-center">Finalizado</span>
                    <div class="d-flex align-items-center justify-content-between">
                        <span class="text-truncate">Equipo C</span>
                        <span class="mx-2 badge bg-secondary text-white">vs</span>
                        <span class="text-truncate">Equipo D</span>
                    </div>
                    <div class="text-center mt-2">
                        <small class="text-muted">11:00 AM</small>
                    </div>
                    <div class="text-center mt-3">
                        <strong>Ganador:</strong> <span class="text-success">Equipo C</span>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <div class="card rounded-md">
                <div class="card-body">
                    <div class="d-flex align-items-center justify-content-between">
                        <span class="text-truncate">Equipo B</span>
                        <span class="mx-2 badge bg-secondary text-white">vs</span>
                        <span class="text-truncate">Equipo C</span>
                    </div>
                    <div class="text-center mt-2">
                        <small class="text-muted">12:00 PM</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 mb-3">
            <div class="card rounded-md">
                <div class="card-body">
                    <div class="d-flex align-items-center justify-content-between">
                        <span class="text-truncate">Equipo D</span>
                        <span class="mx-2 badge bg-secondary text-white">vs</span>
                        <span class="text-truncate">Equipo A</span>
                    </div>
                    <div class="text-center mt-2">
                        <small class="text-muted">01:00 PM</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
