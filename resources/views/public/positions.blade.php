{{-- title --}}
<h4 class="text-center">Tablas de Posiciones</h4>
{{-- end title --}}
<div class="row">
    <div class="col-md-6 p-4">
        <div class="text-center">
            <h4 class="px-2">GRUPO 1</h4>
        </div>
        <div class="table-responsive d-flex justify-content-center">
            <table class="table w-75 bg-white shadow" style="border-radius: 10px;">
                <thead>
                    <tr class="text-white" style="background-color: #c50000c7;border-left: 5px solid #c50000c7;">
                        <th>Equipo</th>
                        <th>J</th>
                        <th>G</th>
                        <th>P</th>
                        <th>E</th>
                        <th>GC</th>
                        <th>Pts</th>
                    </tr>
                </thead>
                <tbody style="font-size: 12px;">
                    <tr style="border-left: 5px solid green;">
                        <td>Equipo sssssssss</td>
                        <td>10</td>
                        <td>7</td>
                        <td>2</td>
                        <td>1</td>
                        <td>2</td>
                        <td>22</td>
                    </tr>
                    <tr style="border-left: 5px solid green;">
                        <td>Equipo B</td>
                        <td>10</td>
                        <td>6</td>
                        <td>3</td>
                        <td>1</td>
                        <td>4</td>
                        <td>19</td>
                    </tr>
                    <tr style="border-left: 5px solid red;">
                        <td>Equipo C</td>
                        <td>10</td>
                        <td>5</td>
                        <td>3</td>
                        <td>2</td>
                        <td>4</td>
                        <td>17</td>
                    </tr>
                    <tr style="border-left: 5px solid red;">
                        <td>Equipo D</td>
                        <td>10</td>
                        <td>4</td>
                        <td>4</td>
                        <td>2</td>
                        <td>6</td>
                        <td>14</td>
                    </tr>
                    <tr style="border-left: 5px solid red;">
                        <td>Equipo E</td>
                        <td>10</td>
                        <td>3</td>
                        <td>5</td>
                        <td>2</td>
                        <td>-3</td>
                        <td>11</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6 p-4">
        <div class="text-center">
            <h4 class=" px-2">GRUPO 1</h4>
        </div>
        <div class="table-responsive d-flex justify-content-center">
            <table class="table w-75 bg-white shadow" style="border-radius: 15px;">
                <thead>
                    <tr class="text-white" style="background-color: #c50000c7;">
                        <th>Equipo</th>
                        <th>J</th>
                        <th>G</th>
                        <th>P</th>
                        <th>E</th>
                        <th>GC</th>
                        <th>Pts</th>
                    </tr>
                </thead>
                <tbody style="font-size: 12px;">
                    <tr>
                        <td>Equipo sssssssss</td>
                        <td>10</td>
                        <td>7</td>
                        <td>2</td>
                        <td>1</td>
                        <td>2</td>
                        <td>22</td>
                    </tr>
                    <tr>
                        <td>Equipo B</td>
                        <td>10</td>
                        <td>6</td>
                        <td>3</td>
                        <td>1</td>
                        <td>4</td>
                        <td>19</td>
                    </tr>
                    <tr>
                        <td>Equipo C</td>
                        <td>10</td>
                        <td>5</td>
                        <td>3</td>
                        <td>2</td>
                        <td>4</td>
                        <td>17</td>
                    </tr>
                    <tr>
                        <td>Equipo D</td>
                        <td>10</td>
                        <td>4</td>
                        <td>4</td>
                        <td>2</td>
                        <td>6</td>
                        <td>14</td>
                    </tr>
                    <tr>
                        <td>Equipo E</td>
                        <td>10</td>
                        <td>3</td>
                        <td>5</td>
                        <td>2</td>
                        <td>-3</td>
                        <td>11</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6 p-4">
        <div class="text-center">
            <h4 class=" px-2">GRUPO 1</h4>
        </div>
        <div class="table-responsive d-flex justify-content-center">
            <table class="table w-75 bg-white shadow" style="border-radius: 15px;">
                <thead>
                    <tr class="text-white" style="background-color: #c50000c7;">
                        <th>Equipo</th>
                        <th>J</th>
                        <th>G</th>
                        <th>P</th>
                        <th>E</th>
                        <th>GC</th>
                        <th>Pts</th>
                    </tr>
                </thead>
                <tbody style="font-size: 12px;">
                    <tr>
                        <td>Equipo sssssssss</td>
                        <td>10</td>
                        <td>7</td>
                        <td>2</td>
                        <td>1</td>
                        <td>2</td>
                        <td>22</td>
                    </tr>
                    <tr>
                        <td>Equipo B</td>
                        <td>10</td>
                        <td>6</td>
                        <td>3</td>
                        <td>1</td>
                        <td>4</td>
                        <td>19</td>
                    </tr>
                    <tr>
                        <td>Equipo C</td>
                        <td>10</td>
                        <td>5</td>
                        <td>3</td>
                        <td>2</td>
                        <td>4</td>
                        <td>17</td>
                    </tr>
                    <tr>
                        <td>Equipo D</td>
                        <td>10</td>
                        <td>4</td>
                        <td>4</td>
                        <td>2</td>
                        <td>6</td>
                        <td>14</td>
                    </tr>
                    <tr>
                        <td>Equipo E</td>
                        <td>10</td>
                        <td>3</td>
                        <td>5</td>
                        <td>2</td>
                        <td>-3</td>
                        <td>11</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6 p-4">
        <div class="text-center">
            <h4 class=" px-2">GRUPO 1</h4>
        </div>
        <div class="table-responsive d-flex justify-content-center">
            <table class="table w-75 bg-white shadow" style="border-radius: 15px;">
                <thead>
                    <tr class="text-white" style="background-color: #c50000c7;">
                        <th>Equipo</th>
                        <th>J</th>
                        <th>G</th>
                        <th>P</th>
                        <th>E</th>
                        <th>GC</th>
                        <th>Pts</th>
                    </tr>
                </thead>
                <tbody style="font-size: 12px;">
                    <tr>
                        <td>Equipo sssssssss</td>
                        <td>10</td>
                        <td>7</td>
                        <td>2</td>
                        <td>1</td>
                        <td>2</td>
                        <td>22</td>
                    </tr>
                    <tr>
                        <td>Equipo B</td>
                        <td>10</td>
                        <td>6</td>
                        <td>3</td>
                        <td>1</td>
                        <td>4</td>
                        <td>19</td>
                    </tr>
                    <tr>
                        <td>Equipo C</td>
                        <td>10</td>
                        <td>5</td>
                        <td>3</td>
                        <td>2</td>
                        <td>4</td>
                        <td>17</td>
                    </tr>
                    <tr>
                        <td>Equipo D</td>
                        <td>10</td>
                        <td>4</td>
                        <td>4</td>
                        <td>2</td>
                        <td>6</td>
                        <td>14</td>
                    </tr>
                    <tr>
                        <td>Equipo E</td>
                        <td>10</td>
                        <td>3</td>
                        <td>5</td>
                        <td>2</td>
                        <td>-3</td>
                        <td>11</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>