@include('layout.nav.head')
@extends('index')

<div class="container my-4">
    <div class="text-center mb-4">
        <img src="{{ asset('static/img/uploads_torneo_logo/'. $tournament->logo) }}" class="img-fluid" alt="{{ $tournament->name }}" style="height: 100px; object-fit: cover;">
        <h1 class="my-4">Información del torneo: {{ $tournament->name }}</h1>
    </div>

    <div class="row">
        <div class="col-md-8">
            <h3>Grupos del torneo</h3>
            <div class="row">
                @foreach($tournament->groups as $group)
                    <div class="col-md-12 mb-4">
                        <div class="card h-100 border-0 shadow">
                            <div class="card-body">
                                <h5 class="card-title">{{ $group->name }}</h5>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Equipo</th>
                                                <th>Pts</th>
                                                <th>PJ</th>
                                                <th>G</th>
                                                <th>GC</th>
                                                <th>Resultado</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($group->groupDetails as $index => $detail)
                                                <tr>
                                                    <td>{{ $index + 1 }}</td>
                                                    <td>{{ $detail->team->team_name }}</td>
                                                    <td>{{ $detail->pts }}</td>
                                                    <td>{{ $detail->pj }}</td>
                                                    <td>{{ $detail->g }}</td>
                                                    <td>{{ $detail->gc }}</td>
                                                    <td>
                                                        @for($i = 0; $i < $detail->won_matches; $i++)
                                                            <span class="badge bg-success me-1"></span>
                                                        @endfor
                                                        @for($i = 0; $i < $detail->lost_matches; $i++)
                                                            <span class="badge bg-danger me-1"></span>
                                                        @endfor
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="col-md-4">
            <h3>Goleadores</h3>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Posición</th>
                            <th>Nombre del Jugador</th>
                            <th>Goles</th>
                            <th>Equipo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tournament->topScorers() as $index => $topScorer)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ $topScorer->first_name }} {{ $topScorer->last_name }}</td>
                                <td>{{ $topScorer->goals }}</td>
                                {{-- <td>{{ $topScorer->team->team_name }}</td> --}}
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
