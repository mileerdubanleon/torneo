<main id="main" class="main">
        <div class="row">
            @foreach($tournaments as $tournament)
                <div class="col-md-4 mb-4">
                    <div class="card h-100 border-0 shadow">
                        <div class="container">
                            @if($tournament->logo)
                                <img src="{{ asset('static/img/uploads_torneo_logo/'. $tournament->logo) }}" class="w-100" alt="{{ $tournament->name }}" style="height: 100px; object-fit: cover;">
                            @else
                                <img src="{{ asset('static/img/default_logo.png') }}" class="w-100" alt="{{ $tournament->name }}" style="height: 100px; object-fit: cover;">
                            @endif
                        </div>
                        <div class="card-body d-flex flex-column">
                            <h5 class="card-title text-center">{{ $tournament->name }}</h5>
                            <span class="card-text my-1">
                                <strong>No. de Equipos:</strong> {{ $tournament->no_teams }}<br>
                                <strong>No. Máx. de Jugadores por Equipo:</strong> {{ $tournament->no_max_players_per_team }}<br>
                                <strong>No. Máx. de Grupos:</strong> {{ $tournament->no_max_groups }}<br>
                                <strong>Lugar:</strong> {{ $tournament->place }}<br>
                                <strong>Descripción:</strong> {{ Str::limit($tournament->description, 100) }}<br>
                                <strong>Estado:</strong> {{ $tournament->status == 1 ? 'Activo' : 'Inactivo' }}
                            </span>
                        </div>
                        <div class="card-footer bg-transparent border-0 d-flex justify-content-center">
                            <a href="{{ url('/tournaments/teams', $tournament->id) }}" class="btn btn-sm btn-primary">
                                Ver Más...
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
</main>