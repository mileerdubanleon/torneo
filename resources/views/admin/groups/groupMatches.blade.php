@extends('admin.index')
@include('layout.nav.head')
@include('admin.layout.sidebar')

<main id="main" class="main">
    <div class="container">
        <h4>Partidos del {{ $group->name }} - Torneo {{ $group->tournament->name }}</h4>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('groups') }}">Grupo</a></li>
                <li class="breadcrumb-item active">{{ $group->name }}: Partidos</li>
            </ol>
        </nav>
        
        {{-- messages error --}}
        @if(Session::has('message'))
            <div class="alert alert-{{ Session::get('typealert') }}">
                {{ Session::get('message') }}
            </div>
        @endif

        <div class="mb-4">
            <!-- Botón para abrir el modal de agregar partido -->
            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#addMatchModal">
                Agregar Partido
            </button>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Equipo 1</th>
                        <th>Equipo 2</th>
                        <th>Hora</th>
                        <th>Resultado</th>
                        <th>Estado</th>
                        <th>Detalles</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($matches as $match)
                        <tr>
                            <td>{{ $match->id }}</td>
                            <td>{{ $match->name }}</td>
                            <td>{{ $match->teamLocal->team_name }}</td>
                            <td>{{ $match->teamVisitor->team_name }}</td>
                            <td>{{ $match->hour }}</td>
                            <td>{{ $match->result_team1 }} - {{ $match->result_team2 }}</td>
                            <td class="{{ getStatusClass($match->status) }}">{{ getStatusText($match->status) }}</td>
                            <td>
                                <a href="{{ route('show_goals', $match->id) }}" class="btn btn-warning">
                                    Goles
                                </a>
                                <a href="{{ route('show_cards', $match->id) }}" class="btn btn-info my-1">
                                    Tarjetas
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('get_matches_edit', $match->id) }}" class="btn btn-success my-1">Editar</a>
                                <form action="" method="POST" style="display:inline;">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Eliminar</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</main>

<!-- Modal -->
<div class="modal fade" id="addMatchModal" tabindex="-1" aria-labelledby="addMatchModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addMatchModalLabel">Agregar Partido</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('add_group_match', $group->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label for="name" class="form-label">Nombre del Partido:</label>
                        <input type="text" name="name" id="name" class="form-control" required>
                    </div>
                    <div class="mb-3">
                        <label for="team_1" class="form-label">Equipo 1:</label>
                        <select name="team_1" id="team_1" class="form-control" required>
                            <option value="" selected disabled>Seleccionar equipo</option>
                            @foreach($group->teams as $team)
                                <option value="{{ $team->id }}">{{ $team->team_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="team_2" class="form-label">Equipo 2:</label>
                        <select name="team_2" id="team_2" class="form-control" required>
                            <option value="" selected disabled>Seleccionar equipo</option>
                            @foreach($group->teams as $team)
                                <option value="{{ $team->id }}">{{ $team->team_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="hour" class="form-label">Hora:</label>
                        <input type="text" name="hour" id="hour" class="form-control" required>
                    </div>
                    <div class="mb-3">
                        <label for="place" class="form-label">Lugar:</label>
                        <input type="text" name="place" id="place" class="form-control" required>
                    </div>
                    <div class="mb-3">
                        <label for="result_team1" class="form-label">Resultado Equipo 1:</label>
                        <input type="number" name="result_team1" id="result_team1" class="form-control">
                    </div>
                    <div class="mb-3">
                        <label for="result_team2" class="form-label">Resultado Equipo 2:</label>
                        <input type="number" name="result_team2" id="result_team2" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-success">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap JS (para manejar el modal) -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
