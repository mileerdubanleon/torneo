@extends('admin.index')
@include('layout.nav.head')
@include('admin.layout.sidebar')

<main id="main" class="main">
    <div class="container">
        {{-- messages error --}}
        @if(Session::has('message'))
            <div class="alert alert-{{ Session::get('typealert') }}">
                {{ Session::get('message') }}
            </div>
        @endif
        <h1>Grupos</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Grupos</li>
            </ol>
        </nav>

        <div class="mb-4">
            <a href="{{ route('get_groups_add') }}" class="btn btn-primary">Agregar Grupo</a>
        </div>
        
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Torneo</th>
                        <th>No. De equipos</th>
                        <th>Equipos</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($groups as $group)
                        <tr>
                            <td>{{ $group->id }}</td>
                            <td>{{ $group->name }}</td>
                            <td>{{ $group->tournament->name }}</td>
                            <td>{{ $group->no_teams }}</td>
                            <td>
                                <a class="btn btn-outline-info" href="{{ url('/admin/groups/teams', $group->id) }}">
                                    Equipos
                                </a>
                                <a href="{{ route('view_group_matches', $group->id) }}" class="btn btn-success">Agregar Partidos</a>
                            </td>
                            <td>
                                <a href="{{ route('get_groups_edit', $group->id) }}" class="btn btn-warning">Editar</a>
                                <form action="{{ url('/admin/groups', $group->id) }}" method="POST" style="display:inline;">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Eliminar</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</main>