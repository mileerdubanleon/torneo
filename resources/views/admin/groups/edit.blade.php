<!-- resources/views/admin/groups/edit.blade.php -->
@extends('admin.index')
@include('layout.nav.head')
@include('admin.layout.sidebar')

<main id="main" class="main">
    <div class="container">
        <h1>Editar Grupo</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('groups') }}">Grupos</a></li>
                <li class="breadcrumb-item active">Editar Grupo</li>
            </ol>
        </nav>

        <form action="{{ url('/admin/groups/edit', $group->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="mb-3">
                <label for="name" class="form-label">Nombre del Grupo</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $group->name }}" required>
            </div>
            <div class="mb-3">
                <label for="name" class="form-label">Numero de Equipos maximo</label>
                <input type="number" class="form-control" id="team_max" name="team_max" value="{{ $group->no_teams }}" required>
            </div>
            <div class="mb-3">
                <label for="tournament_id" class="form-label">Torneo</label>
                <select class="form-control" id="tournament_id" name="tournament_id" required>
                    @foreach($tournaments as $tournament)
                        <option value="{{ $tournament->id }}" @if($tournament->id == $group->tournament_id) selected @endif>{{ $tournament->name }}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Actualizar Grupo</button>
        </form>
    </div>
</main>
