@extends('admin.index')
@include('layout.nav.head')
@include('admin.layout.sidebar')

<main id="main" class="main">
    <div class="container">
        {{-- messages error --}}
        @if(Session::has('message'))
            <div class="alert alert-{{ Session::get('typealert') }}">
                {{ Session::get('message') }}
            </div>
        @endif
        <h1>Agregar Grupo</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('groups') }}">Grupos</a></li>
                <li class="breadcrumb-item active">Agregar Grupo</li>
            </ol>
        </nav>

        <form action="{{ url('/admin/groups/add') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="mb-3">
                <label for="name" class="form-label">Nombre del Grupo</label>
                <input type="text" class="form-control" id="name" name="name" required>
            </div>
            <div class="mb-3">
                <label for="name" class="form-label">Numero de Equipos maximo</label>
                <input type="number" class="form-control" id="team_max" name="team_max" required>
            </div>
            <div class="mb-3">
                <label for="tournament_id" class="form-label">Torneo</label>
                <select class="form-control" id="tournament_id" name="tournament_id" required>
                    @foreach($tournaments as $tournament)
                        <option value="{{ $tournament->id }}">{{ $tournament->name }}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Guardar Grupo</button>
        </form>
    </div>
</main>