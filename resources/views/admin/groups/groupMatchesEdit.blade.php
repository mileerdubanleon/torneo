@extends('admin.index')
@include('layout.nav.head')
@include('admin.layout.sidebar')

<main id="main" class="main">
    <div class="container">
        <h1>Editar Partido</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Editar Partido</li>
            </ol>
        </nav>
        <div class="">
            <div class="header">
                <h2 class="title"><i class="fas fa-edit"></i> Editar Partido</h2>
            </div>

            <div class="inside">
                <form action="{{ route('post_matches_edit', $match->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <label for="name">Nombre del Partido:</label>
                            <input type="text" name="name" id="name" class="form-control" value="{{ $match->name }}" required>
                        </div>
                        <div class="col-md-6">
                            <label for="team_1">Equipo 1:</label>
                            <select disabled name="team_1" id="team_1" class="form-control" required>
                                <option value="" selected disabled>Seleccionar equipo</option>
                                @foreach($teams as $id => $name)
                                    <option value="{{ $id }}" {{ $match->team_1 == $id ? 'selected' : '' }}>{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="team_2">Equipo 2:</label>
                            <select disabled name="team_2" id="team_2" class="form-control" required>
                                <option value="" selected disabled>Seleccionar equipo</option>
                                @foreach($teams as $id => $name)
                                    <option value="{{ $id }}" {{ $match->team_2 == $id ? 'selected' : '' }}>{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="hour">Hora:</label>
                            <input type="text" name="hour" id="hour" class="form-control" value="{{ $match->hour }}" required>
                        </div>
                        <div class="col-md-6">
                            <label for="place">Lugar:</label>
                            <input type="text" name="place" id="place" class="form-control" value="{{ $match->place }}" required>
                        </div>
                        <div class="mb-3">
                            <label for="result_team1" class="form-label">Resultado Equipo 1:</label>
                            <input type="number" name="result_team1" value="{{ $match->result_team1 }}" id="result_team1" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label for="result_team2" class="form-label">Resultado Equipo 2:</label>
                            <input type="number" name="result_team2" value="{{ $match->result_team2 }}" id="result_team2" class="form-control">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success mt-3">Guardar</button>
                </form>
                
            </div>
        </div>
    </div>
</main>
