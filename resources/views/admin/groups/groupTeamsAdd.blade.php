@extends('admin.index')
@include('layout.nav.head')
@include('admin.layout.sidebar')

<main id="main" class="main">
    <div class="container">
        {{-- messages error --}}
        @if(Session::has('message'))
            <div class="alert alert-{{ Session::get('typealert') }}">
                {{ Session::get('message') }}
            </div>
        @endif
        <h1>Agregar Grupo</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('groups') }}">Grupos</a></li>
                <li class="breadcrumb-item active">Agregar Grupo</li>
            </ol>
        </nav>
        <div class="">
            <div class="header">
                <h2 class="title"><i class="fas fa-plus"></i> Agregar Equipo al Grupo: {{ $group->name }}</h2>
            </div>

            <div class="inside">
                <form action="{{ url('/admin/groups/teams/'.$group->id.'/add') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <label for="team_id">Seleccionar Equipo:</label>
                            <select name="team_id" id="team_id" class="form-select">
                                <option value="" selected disabled>Seleccionar equipo</option>
                                @foreach($teams as $id => $name)
                                    <option value="{{ $id }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success mt-3">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</main>
