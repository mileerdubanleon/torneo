@extends('admin.index')
@include('layout.nav.head')
@include('admin.layout.sidebar')

<main id="main" class="main">
    <div class="container">
        <h1>Equipos del Grupo: {{ $group->name }}</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('groups') }}">Grupos</a></li>
                <li class="breadcrumb-item active">Equipos</li>
            </ol>
        </nav>

        <div class="mb-4">
            <a href="{{ url('/admin/groups/teams/'.$group->id.'/add') }}" class="btn btn-primary">Agregar Equipo</a>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Equipo</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($group->details as $detail)
                        <tr>
                            <td>{{ $detail->team->id }}</td>
                            <td>{{ $detail->team->team_name }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</main>
