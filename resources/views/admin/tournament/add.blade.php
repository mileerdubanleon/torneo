@extends('admin.index')

{{-- Cabecera web --}}
@include('layout.nav.head')

{{-- sidebar --}}
@include('admin.layout.sidebar')

{{-- content --}}
<main id="main" class="main">
    <div class="container">
        {{-- messages error --}}
        @if(Session::has('message'))
            <div class="alert alert-{{ Session::get('typealert') }}">
                {{ Session::get('message') }}
            </div>
        @endif
        <!-- Page Title -->
        <div class="pagetitle">
            <h1>Agregar torneo</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Agregar torneo</li>
                </ol>
            </nav>
        </div>
        <!-- End Page Title -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Crear Nuevo Torneo</h4>
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @if(Session::has('message'))
                                <div class="alert alert-{{ Session::get('typealert') }}">
                                    {{ Session::get('message') }}
                                </div>
                            @endif

                            <form action="{{ url('/admin/tournaments/add') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group mb-3">
                                    <label for="name">Nombre del Torneo</label>
                                    <input type="text" class="form-control" id="name" name="name" required>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="no_teams">Número de Equipos</label>
                                    <input type="number" class="form-control" id="no_teams" name="no_teams" required>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="no_max_players_per_team">Número Máximo de Jugadores por Equipo</label>
                                    <input type="number" class="form-control" id="no_max_players_per_team" name="no_max_players_per_team" required>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="no_max_groups">Número Máximo de Grupos</label>
                                    <input type="number" class="form-control" id="no_max_groups" name="no_max_groups" required>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="place">Lugar</label>
                                    <input type="text" class="form-control" id="place" name="place" required>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="description">Descripción</label>
                                    <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="status">Estado</label>
                                    <select class="form-control" id="status" name="status" required>
                                        <option value="activo">Activo</option>
                                        <option value="inactivo">Inactivo</option>
                                    </select>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="logo">Logo del Torneo</label>
                                    <input type="file" class="form-control" id="logo" name="logo" accept="image/*">
                                </div>
                                <button type="submit" class="btn btn-primary">Guardar Torneo</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
