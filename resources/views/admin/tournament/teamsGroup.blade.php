@extends('admin.index')

{{-- Cabecera web --}}
@include('layout.nav.head')

{{-- sidebar --}}
@include('admin.layout.sidebar')

{{-- content --}}
<main id="main" class="main">
    <div class="container">
        {{-- messages error --}}
        @if(Session::has('message'))
            <div class="alert alert-{{ Session::get('typealert') }}">
                {{ Session::get('message') }}
            </div>
        @endif
        <!--Page Title -->
        <div class="pagetitle m-0 p-0">
            <h1>Grupos - Equipos</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('/admin/tournaments') }}">Torneos</a></li>
                    <li class="breadcrumb-item active">Grupos</li>
                </ol>
            </nav>
        </div>
        <!-- End Page Title -->
        
        <div class="container-fluid">
            <div class="row">

                <div class="table-responsive">
                    <div class="col-md-8">
                        <h3>Grupos del torneo</h3>
                        <div class="row">
                            @foreach($tournament->groups as $group)
                                <div class="col-md-12 mb-4">
                                    <div class="card h-100 border-0 shadow">
                                        <div class="card-body">
                                            <h5 class="card-title">{{ $group->name }}</h5>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Equipo</th>
                                                            <th>Pts</th>
                                                            <th>PJ</th>
                                                            <th>G</th>
                                                            <th>GC</th>
                                                            <th>Resultado</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($group->groupDetails as $index => $detail)
                                                            <tr>
                                                                <td>{{ $index + 1 }}</td>
                                                                <td>{{ $detail->team->team_name }}</td>
                                                                <td>{{ $detail->pts }}</td>
                                                                <td>{{ $detail->pj }}</td>
                                                                <td>{{ $detail->g }}</td>
                                                                <td>{{ $detail->gc }}</td>
                                                                <td>
                                                                    @for($i = 0; $i < $detail->won_matches; $i++)
                                                                        <span class="badge bg-success me-1"></span>
                                                                    @endfor
                                                                    @for($i = 0; $i < $detail->lost_matches; $i++)
                                                                        <span class="badge bg-danger me-1"></span>
                                                                    @endfor
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</main>
