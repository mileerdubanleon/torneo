@extends('admin.index')

{{-- Cabecera web --}}
@include('layout.nav.head')

{{-- sidebar --}}
@include('admin.layout.sidebar')

{{-- content --}}
<main id="main" class="main">
    <div class="container">
        {{-- messages error --}}
        @if(Session::has('message'))
            <div class="alert alert-{{ Session::get('typealert') }}">
                {{ Session::get('message') }}
            </div>
        @endif
        <!--Page Title -->
        <div class="pagetitle m-0 p-0">
            <h1>Torneo</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Torneo</li>
                </ol>
            </nav>
        </div>
        <!-- End Page Title -->
        
        <div class="container-fluid">
            <div class="row mb-3">
                <div class="col-md-12 text-left">
                    <a href="{{ url('/admin/tournaments/add') }}" class="btn btn-primary">Agregar Torneo</a>
                </div>
            </div>
            <div class="row">
                @foreach($tournaments as $tournament)
                    <div class="col-md-4 mb-4">
                        <div class="card h-100 border-0 shadow">
                            <div class="container">
                                @if($tournament->logo)
                                    <img src="{{ asset('static/img/uploads_torneo_logo/'. $tournament->logo) }}"  class="w-100" alt="{{ $tournament->name }}" style="height: 100px; object-fit: cover;">
                                @else
                                    <img src="{{ asset('static/img/uploads_torneo_logo/'. $tournament->logo) }}" class="w-100" alt="{{ $tournament->name }}" style="height: 100px; object-fit: cover;">
                                @endif
                            </div>
                            <div class="card-body d-flex flex-column">
                                <h5 class="card-title text-center">{{ $tournament->name }}</h5>
                                <span class="card-text my-1">
                                    <strong>No. de Equipos:</strong> {{ $tournament->no_teams }}<br>
                                    <strong>No. Máx. de Jugadores por Equipo:</strong> {{ $tournament->no_max_players_per_team }}<br>
                                    <strong>No. Máx. de Grupos:</strong> {{ $tournament->no_max_groups }}<br>
                                    <strong>Lugar:</strong> {{ $tournament->place }}<br>
                                    <strong>Descripción:</strong> {{ Str::limit($tournament->description, 100) }}<br>
                                    <strong>Estado:</strong> {{ $tournament->status == 1 ? 'Activo' : 'Inactivo' }}
                                </span>
                            </div>
                            <div class="card-footer bg-transparent border-0 d-flex justify-content-between">
                                <a href="{{ url('/admin/tournaments/edit', $tournament->id) }}" class="btn btn-sm btn-primary">Editar</a>
                                {{-- groups --}}
                                <a href="{{ url('/admin/tournaments/teams', $tournament->id) }}" class="btn btn-sm btn-success">
                                    Grupos
                                </a>
                                <form action="{{ url('/admin/tournaments/delete', $tournament->id) }}" method="POST" style="display:inline;">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-danger">Eliminar</button>
                                </form>
                            </div>
                        </div>
                        
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</main>
