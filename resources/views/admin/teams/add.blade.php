@extends('admin.index')

{{-- Cabecera web --}}
@include('layout.nav.head')

{{-- sidebar --}}
@include('admin.layout.sidebar')

{{-- content --}}
<main id="main" class="main">
    <div class="container">
        {{-- messages error --}}
        @if(Session::has('message'))
            <div class="alert alert-{{ Session::get('typealert') }}">
                {{ Session::get('message') }}
            </div>
        @endif
        <!-- Page Title -->
        <div class="pagetitle">
            <h1>Agregar equipo</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Agregar equipos</li>
                </ol>
            </nav>
        </div>
        <!-- End Page Title -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Crear Nuevo Equipo</h4>
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @if(Session::has('message'))
                                <div class="alert alert-{{ Session::get('typealert') }}">
                                    {{ Session::get('message') }}
                                </div>
                            @endif

                            <form action="{{ url('/admin/teams/add') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group mb-3">
                                    <label for="team_name">Nombre del Equipo</label>
                                    <input type="text" class="form-control" id="team_name" name="team_name" required>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="uniform">Uniforme</label>
                                    <input type="text" class="form-control" id="uniform" name="uniform" required>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="description">Descripción del Equipo</label>
                                    <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="logo">Logo del Equipo</label>
                                    <input type="file" class="form-control" id="logo" name="logo" accept="image/*">
                                </div>
                                <div class="form-group mb-3">
                                    <label for="status">Estado</label>
                                    <select class="form-control" id="status" name="status" required>
                                        <option value="1">Activo</option>
                                        <option value="0">Inactivo</option>
                                    </select>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="tournament_id">Torneo</label>
                                    <select class="form-control" id="tournament_id" name="tournament_id">
                                        <option value="">Seleccione un Torneo</option>
                                        @foreach($tournaments as $tournament)
                                            <option value="{{ $tournament->id }}">{{ $tournament->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary">Guardar Equipo</button>
                            </form>
                            

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>