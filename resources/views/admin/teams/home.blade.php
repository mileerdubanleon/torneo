@extends('admin.index')
{{-- Cabecera web --}}
@include('layout.nav.head')
{{-- sidebar --}}
@include('admin.layout.sidebar')
{{-- content --}}

<main id="main" class="main">
    <div class="container">
        {{-- messages error --}}
        @if(Session::has('message'))
            <div class="alert alert-{{ Session::get('typealert') }}">
                {{ Session::get('message') }}
            </div>
        @endif
        <!--Page Title -->
        <div class="">
            <h1>Equipos</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Equipos</li>
                </ol>
            </nav>
        </div>

        <div class="container-fluid">
            <div class="mb-4">
                <a href="{{ url('/admin/teams/add') }}" class="btn btn-primary">Agregar Equipo</a>
            </div>
            <div class="row">
                @foreach ($teams as $team)
                    <div class="col-md-4 mb-4">
                        <div class="card h-100 border-0 shadow-sm p-1">
                            <div class="position-relative">
                                @if($team->logo)
                                    <img src="{{ asset('static/img/uploads_team_logo/'. $team->logo) }}" class="card-img-top rounded" alt="{{ $team->team_name }}" style="border-radius: 15px; width: 100px; height: 100px;">
                                @else
                                    <img src="{{ asset('static/img/uploads_team_logo/default_team_logo.png') }}" class="card-img-top rounded" alt="{{ $team->team_name }}" style="border-radius: 15px;">
                                @endif
                                <div class="card-img-overlay d-flex align-items-center justify-content-end">
                                    <button type="button" class="btn btn-light" data-bs-toggle="modal" data-bs-target="#teamModal{{ $team->id }}">
                                        Ver Información
                                    </button>
                                </div>
                                <h5 class="modal-title p-2 " id="teamModalLabel{{ $team->id }}">{{ $team->team_name }}</h5>
                            </div>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="teamModal{{ $team->id }}" tabindex="-1" aria-labelledby="teamModalLabel{{ $team->id }}" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="teamModalLabel{{ $team->id }}">{{ $team->team_name }}</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">Cerrar</button>
                                </div>
                                <div class="modal-body">
                                    @if($team->tournament && $team->tournament->logo)
                                        <img src="{{ asset('static/img/uploads_torneo_logo/' . $team->tournament->logo) }}" class="img-fluid mb-3" alt="{{ $team->tournament->name }}" style="width: 50px;">
                                    @endif
                                    <p><strong>Torneo:</strong> {{ $team->tournament ? $team->tournament->name : 'No asignado' }}</p>
                                    <p><strong>Uniforme:</strong> {{ $team->uniform }}</p>
                                    <p><strong>Descripción:</strong> {{ $team->description }}</p>
                                    <p><strong>Estado:</strong> {{ $team->status ? 'Activo' : 'Inactivo' }}</p>
                                    <a href="{{ url('/admin/teams/edit', $team->id) }}" class="btn btn-primary">Editar</a>
                                    <a href="{{ url('/admin/teams/delete', $team->id) }}" class="btn btn-danger">Eliminar</a>
                                </div>
                                <div class="modal-footer">
                                    <a type="button" class="btn btn-light text-black" data-bs-dismiss="modal" aria-label="Close">Cerrar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</main>

{{-- Bootstrap 5 Modal and Tooltip Scripts --}}
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"></script>
