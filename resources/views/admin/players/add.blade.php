@extends('admin.index')

{{-- Cabecera web --}}
@include('layout.nav.head')

{{-- sidebar --}}
@include('admin.layout.sidebar')

{{-- content --}}
<main id="main" class="main">
    <div class="container">
        {{-- messages error --}}
        @if(Session::has('message'))
            <div class="alert alert-{{ Session::get('typealert') }}">
                {{ Session::get('message') }}
            </div>
        @endif
        <!-- Page Title -->
        <div class="pagetitle">
            <h1>Agregar Jugador</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Agregar Jugador</li>
                </ol>
            </nav>
        </div>
        <!-- End Page Title -->

        <div class="container-fluid">
            <div class="row p-4 justify-content-center">
                <div class="card col-md-10 p-5">
                    <form action="{{ url('/admin/players/add') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-4 mb-3">
                                <label for="first_name">Nombre</label>
                                <input type="text" class="form-control" id="first_name" name="first_name" required>
                            </div>
                            <div class="form-group col-md-4 mb-3">
                                <label for="last_name">Apellido</label>
                                <input type="text" class="form-control" id="last_name" name="last_name" required>
                            </div>
                            <div class="form-group col-md-4 mb-3">
                                <label for="team_id">Equipo</label>
                                <select class="form-control" id="team_id" name="team_id" required>
                                    @foreach($teams as $team)
                                        <option value="{{ $team->id }}">{{ $team->team_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4 mb-3">
                                <label for="role">Rol</label>
                                <input type="text" class="form-control" id="role" name="role" required>
                            </div>
                            <div class="form-group col-md-4 mb-3">
                                <label for="document_type">Tipo de Documento</label>
                                <input type="number" class="form-control" id="document_type" name="document_type" required>
                            </div>
                            <div class="form-group col-md-4 mb-3">
                                <label for="identification">Identificación</label>
                                <input type="number" class="form-control" id="identification" name="identification" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4 mb-3">
                                <label for="dorsal">Dorsal</label>
                                <input type="number" class="form-control" id="dorsal" name="dorsal" required>
                            </div>
                            <div class="form-group col-md-4 mb-3">
                                <label for="phone">Teléfono</label>
                                <input type="text" class="form-control" id="phone" name="phone" required>
                            </div>
                            <div class="form-group col-md-4 mb-3">
                                <label for="status">Estado</label>
                                <select class="form-control" id="status" name="status" required>
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Guardar Jugador</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>

