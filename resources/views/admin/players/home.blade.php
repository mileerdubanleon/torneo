@extends('admin.index')
{{-- Cabecera web --}}
@include('layout.nav.head')
{{-- sidebar --}}
@include('admin.layout.sidebar')
{{-- content --}}

<main id="main" class="main">
    <div class="container">
        {{-- messages error --}}
        @if(Session::has('message'))
            <div class="alert alert-{{ Session::get('typealert') }}">
                {{ Session::get('message') }}
            </div>
        @endif
        
        <!--Page Title -->
        <div class="">
            <h1>Jugadores</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Jugadores</li>
                </ol>
            </nav>
        </div>

        <div class="container-fluid">
            <div class="mb-4">
                <a href="{{ url('/admin/players/add') }}" class="btn btn-primary">Agregar Jugador</a>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Equipo</th>
                                    <th>Rol</th>
                                    <th>Dorsal</th>
                                    <th>Teléfono</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($players as $player)
                                    <tr>
                                        <td>{{ $player->first_name }}</td>
                                        <td>{{ $player->last_name }}</td>
                                        <td>{{ $player->team ? $player->team->team_name : 'No asignado' }}</td>
                                        <td>{{ $player->role }}</td>
                                        <td>{{ $player->dorsal }}</td>
                                        <td>{{ $player->phone }}</td>
                                        <td>{{ $player->status ? 'Activo' : 'Inactivo' }}</td>
                                        <td>
                                            <a href="{{ url('/admin/players/edit', $player->id) }}" class="btn btn-primary btn-sm">Editar</a>
                                            <form action="{{ url('/admin/players/delete', $player->id) }}" method="POST" style="display:inline;">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div> <!-- End table-responsive -->
                </div>
            </div>
        </div>
    </div>
</main>