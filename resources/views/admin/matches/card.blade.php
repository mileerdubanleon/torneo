@extends('admin.index')
@include('layout.nav.head')
@include('admin.layout.sidebar')

<main id="main" class="main">
    <div class="container">
        <h4>Tarjetas del Partido: {{ $match->name }}</h4>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('groups') }}">Grupos</a></li>
                <li class="breadcrumb-item"><a href="{{ route('view_group_matches', $match->group_id) }}">Partidos</a></li>
                <li class="breadcrumb-item active">Tarjetas del Partido</li>
            </ol>
        </nav>

        <!-- Formulario para agregar tarjeta -->
        <div class="mb-4">
            <form action="{{ route('add_card', $match->id) }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-3 mb-3">
                        <label for="team_id" class="form-label">Equipo:</label>
                        <select name="team_id" id="team_id" class="form-control" required>
                            <option value="" selected disabled>Seleccionar equipo</option>
                            <option value="{{ $match->team_1 }}">{{ $match->teamLocal->team_name }}</option>
                            <option value="{{ $match->team_2 }}">{{ $match->teamVisitor->team_name }}</option>
                        </select>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="player_id" class="form-label">Jugador:</label>
                        <select name="player_id" id="player_id" class="form-control" required>
                            <option value="" selected disabled>Seleccionar jugador</option>
                            <!-- Aquí se cargarán dinámicamente los jugadores según el equipo seleccionado -->
                        </select>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="type" class="form-label">Tipo de Tarjeta:</label>
                        <select name="type" id="type" class="form-control" required>
                            <option value="" selected disabled>Seleccionar tipo</option>
                            <option value="yellow">Amarilla</option>
                            <option value="red">Roja</option>
                        </select>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="minute" class="form-label">Minuto:</label>
                        <input type="number" name="minute" id="minute" class="form-control" required>
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Guardar</button>
            </form>
        </div>

        <!-- Tabla de Tarjetas -->
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Equipo</th>
                        <th>Jugador</th>
                        <th>Tipo</th>
                        <th>Estado</th>
                        <th>Minuto</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($match->cards as $card)
                        <tr id="card-{{ $card->id }}">
                            <td>{{ $card->id }}</td>
                            <td>{{ $card->team->team_name }}</td>
                            <td>{{ $card->player->first_name }} {{ $card->player->last_name }}</td>
                            <td>{{ ucfirst($card->type) }}</td>
                            <td>
                                <label class="switch">
                                    <input type="checkbox" class="toggle-state" data-id="{{ $card->id }}" {{ $card->state == 1 ? 'checked' : '' }}>
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td>{{ $card->minute }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</main>

<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 34px;
        height: 20px;
    }

    .switch input {
        display: none;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        transition: .4s;
        border-radius: 34px;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 14px;
        width: 14px;
        left: 3px;
        bottom: 3px;
        background-color: white;
        transition: .4s;
        border-radius: 50%;
    }

    input:checked + .slider {
        background-color: #4CAF50; /* Verde cuando pagado */
    }

    input + .slider {
        background-color: #ff0000; /* Rojo cuando no pagado */
    }

    input:checked + .slider:before {
        transform: translateX(14px);
    }

    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {
        const playersByTeam = @json($playersByTeam);

        $('#team_id').on('change', function() {
            var teamId = $(this).val();
            var playerSelect = $('#player_id');

            playerSelect.empty().append('<option value="" selected disabled>Seleccionar jugador</option>');

            if (teamId && playersByTeam[teamId]) {
                playersByTeam[teamId].forEach(function(player) {
                    playerSelect.append('<option value="' + player.id + '">' + player.first_name + ' ' + player.last_name + '</option>');
                });
            } else {
                console.log('No se encontraron jugadores para el equipo seleccionado.');
            }
        });

        // Disparar el evento change al cargar la página para cargar los jugadores si hay un equipo seleccionado inicialmente
        $('#team_id').trigger('change');

        // Manejar el interruptor de estado
        $('.toggle-state').change(function() {
            var cardId = $(this).data('id');
            var newState = this.checked ? 1 : 0;

            $.ajax({
                url: '/admin/cards/' + cardId + '/toggle-state',
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: { state: newState },
                success: function(response) {
                    var badge = newState == 1 ? '<span class="badge bg-success">Pagada</span>' : '<span class="badge bg-danger">No pagada</span>';
                    $('#card-' + cardId + ' .badge').html(badge);
                },
                error: function(xhr, status, error) {
                    console.error('Error al cambiar el estado:', error);
                    alert('Error al cambiar el estado.');
                }
            });
        });
    });
</script>
