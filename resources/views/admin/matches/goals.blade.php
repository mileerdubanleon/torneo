@extends('admin.index')
@include('layout.nav.head')
@include('admin.layout.sidebar')

    <main id="main" class="main">
        <div class="container">
            <h4>Goles del Partido: {{ $match->name }}</h4>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('groups') }}">Grupos</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('view_group_matches', $match->group_id) }}">Partidos</a></li>
                    <li class="breadcrumb-item active">Goles del Partido</li>
                </ol>
            </nav>

            <!-- Formulario para agregar gol -->
            <div class="mb-4">
                <form action="{{ route('add_goal', $match->id) }}" method="POST">
                    @csrf
                    <input type="hidden" name="match_id" value="{{ $match->id }}">
                    <div class="row">
                        <div class="col-md-3 mb-3">
                            <label for="goal_team_id" class="form-label">Equipo:</label>
                            <select name="team_id" id="goal_team_id" class="form-control" required>
                                <option value="" selected disabled>Seleccionar equipo</option>
                                <option value="{{ $match->team_1 }}">{{ $match->teamLocal->team_name }}</option>
                                <option value="{{ $match->team_2 }}">{{ $match->teamVisitor->team_name }}</option>
                            </select>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="goal_player_id" class="form-label">Jugador:</label>
                            <select name="player_id" id="goal_player_id" class="form-control" required>
                                <option value="" selected disabled>Seleccionar jugador</option>
                            </select>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="minute" class="form-label">Minuto:</label>
                            <input type="number" name="minute" id="minute" class="form-control" required>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label>&nbsp;</label>
                            <button type="submit" class="btn btn-success form-control">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>

            <!-- Tabla de goles -->
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Equipo</th>
                            <th>Jugador</th>
                            <th>Minuto</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($match->goals as $goal)
                            <tr>
                                <td>{{ $goal->id }}</td>
                                <td>{{ $goal->team->team_name }}</td>
                                <td>{{ $goal->player->first_name }} {{ $goal->player->last_name }}</td>
                                <td>{{ $goal->minute }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </main>

    <!-- Asegúrate de incluir jQuery -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <script>
        $(document).ready(function() {
            const playersByTeam = @json($playersByTeam);

            $('#goal_team_id').on('change', function() {
                const selectedTeamId = $(this).val();
                const playerSelect = $('#goal_player_id');

                playerSelect.empty().append('<option value="" selected disabled>Seleccionar jugador</option>');

                if (selectedTeamId && playersByTeam[selectedTeamId]) {
                    playersByTeam[selectedTeamId].forEach(function(player) {
                        playerSelect.append('<option value="' + player.id + '">' + player.first_name + ' ' + player.last_name + '</option>');
                    });
                } else {
                    console.log('No se encontraron jugadores para el equipo seleccionado.');
                }
            });

            // Disparar el evento change al cargar la página para cargar los jugadores si hay un equipo seleccionado inicialmente
            $('#goal_team_id').trigger('change');
        });
    </script>
