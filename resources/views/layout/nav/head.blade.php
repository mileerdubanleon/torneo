<div class="d-flex container-fluid container-movile">

    @if (Auth::check())
        <div class="d-flex container-fluid" >
            <div class="container-header">
                <a class="link-item" href="{{ url('/') }}">
                    Inicio
                </a><span>|</span>
                <a class="link-item " href="#">Quiénes somos</a>
                <span>|</span>

                <a class="link-item " href="#">WhatsApp</a>
                <span>|</span>
                <a class="dropdown ">
                    <a class="dropdown-toggle p-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ Auth::user()->name }} 
                        <span>

                        </span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <!-- Agrega aquí las opciones del dropdown -->
                        <a class="dropdown-item" href="{{ url('/account/edit') }}">Cuenta</a>
                        <a class="dropdown-item" href="#">Configuración</a>
                        @if (Auth::user()->role == 1)
                            <a class="dropdown-item" href="{{ url('/admin') }}">Administración</a>
                        @endif
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ url('/logout') }}">Cerrar sesión</a>
                    </div>
                </a>
                
            </div>
        </div>
    @else

        <div class="container-header">
            <a class="link-item " href="#">Quiénes somos</a>
            <span>|</span>

            <a class="link-item " href="#">Contacto</a>
            <span>|</span>

            <a class="link-item " href="#">Ayuda</a>
        </div>
    @endif

    
</div>

<div class="order-md-last d-flex my-2 navbar navbar-expand-md navbar-light">
    <a href="/">
        <span>Torneo Micro Relampago</span>
    </a>
    {{-- name user, menu responsive --}}
    @if (Auth::check())
        <span class="dropdown cart-style-responsive droptown-movile" style="margin-right: 0px;">
            <a class="dropdown-toggle p-2 prop-user mx-1 "  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ Auth::user()->name }} 
                <span>

                </span>
            </a>
            <div class="dropdown-menu position-absolute" aria-labelledby="dropdownMenuButton">
                <!-- Agrega aquí las opciones del dropdown -->
                <a class="dropdown-item" href="{{ url('/account/edit') }}">Cuenta</a>
                <a class="dropdown-item" href="#">Configuración</a>
                @if (Auth::user()->role == 1)
                    <a class="dropdown-item" href="{{ url('/admin') }}">Administración</a>
                @endif
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ url('/logout') }}">Cerrar sesión</a>
            </div>
        </span>
    @else 
    {{-- icon responsive sesion --}}
    @endif

    
</div>
