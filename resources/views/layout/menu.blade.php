
<div class="container-fluid mb-5">
    <div class="row">
        {{-- sidebar --}}
        <div class="col-md-3">
            {{-- Sidebar --}}
            <div>
                <ul class="my-4">
                    <li class="list-group-item">
                        <a href="{{ url('/tournaments') }}" class="sidebar-link" data-section="dashboardhome">Torneos</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" class="sidebar-link" data-section="position">Posiciones</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" class="sidebar-link" data-section="match">Partidos</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" class="sidebar-link" data-section="team">Equipos</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" class="sidebar-link" data-section="rule">Normas</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" class="sidebar-link" data-section="statistics">Estadística</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-md-7">
            {{-- Contenido dinámico --}}
            <div id="dynamic-content" class="my-4">
                <div class="section" id="dashboardhome">
                        @include('public.tournament')
                </div>
                <div class="section" id="position">
                    {{-- *******POSITION****** --}}
                    <div class="section" id="position">
                        @include('public.positions')
                    </div>
                </div>
                <div class="section" id="match" style="display: none;">
                    <!-- Contenido de partidos -->
                    @include('public.matcher')
                    <!-- Aquí va la información de partidos -->
                </div>
                <div class="section" id="team" style="display: none;">
                    <!-- Contenido  -->
                    <h4>team</h4>
                    <!-- Aquí va la información de tarjetas de crédito -->
                </div>
                <div class="section" id="rule" style="display: none;">
                    <!-- Contenido  -->
                    <h4>rule</h4>
                    <!-- Aquí va la información de favoritos -->
                </div>
                <div class="section" id="statistics" style="display: none;">
                    <!-- Contenido de Direcciones -->
                    <h4>statistics</h4>
                    <!-- Aquí va la información de direcciones -->
                </div>
            </div>
        </div>
    </div>
</div>
{{-- styles --}}
<style>

    .sidebar-link {
		color: #000000;
        font-weight: normal;
		text-decoration: none !important;
    }

    .sidebar-link.active {
        font-weight: bold;
        border-bottom: 2px solid #000000;
    }
</style>